-- The ipbus bus fabric, address select logic, data multiplexers
--
-- This version selects the addressed slave depending on the state
-- of incoming control lines
--
-- Dave Newbold, February 2011

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.ipbus.ALL;

entity ipbus_fabric_sel is
  generic(
    NSLV: positive;
    STROBE_GAP: boolean := false;
    SEL_WIDTH: positive;
    GENERATE_ILA: boolean := false
   );
  port(
  	clk: in std_logic := '0';
  	sel: in std_logic_vector(SEL_WIDTH - 1 downto 0);
    ipb_in: in ipb_wbus;
    ipb_out: out ipb_rbus;
    ipb_to_slaves: out ipb_wbus_array(NSLV - 1 downto 0);
    ipb_from_slaves: in ipb_rbus_array(NSLV - 1 downto 0) := (others => IPB_RBUS_NULL)
   );

end ipbus_fabric_sel;

architecture rtl of ipbus_fabric_sel is

COMPONENT ila_0
PORT (
	clk : IN STD_LOGIC;
	probe0 : IN STD_LOGIC_VECTOR(67 DOWNTO 0)
);
END COMPONENT  ;

	signal sel_i: integer range 0 to NSLV := 0;
	signal true_ack, true_err, qstrobe: std_logic;

	signal ored_ack: std_logic_vector(NSLV downto 0);
	signal probes: std_logic_vector(67 downto 0) := (others => '0');

begin

	sel_i <= to_integer(unsigned(sel));

	qstrobe <= ipb_in.ipb_strobe when STROBE_GAP = false else
	 ipb_in.ipb_strobe and not (true_ack or true_err);

	ored_ack(NSLV) <= '0';

	busgen: for i in NSLV-1 downto 0 generate
	begin

		ipb_to_slaves(i).ipb_addr <= ipb_in.ipb_addr;
		ipb_to_slaves(i).ipb_wdata <= ipb_in.ipb_wdata;
		ipb_to_slaves(i).ipb_strobe <= qstrobe when sel_i = i else '0';
		ipb_to_slaves(i).ipb_write <= ipb_in.ipb_write;

		ored_ack(i) <= ored_ack(i+1) or ipb_from_slaves(i).ipb_ack;

	end generate;

  true_ack <= ipb_from_slaves(sel_i).ipb_ack when sel_i /= NSLV else '0';
  true_err <= ipb_from_slaves(sel_i).ipb_err when sel_i /= NSLV else '0';
  ipb_out.ipb_rdata <= ipb_from_slaves(sel_i).ipb_rdata when sel_i /= NSLV else (others => '0');
  ipb_out.ipb_ack <= true_ack;
  ipb_out.ipb_err <= true_err;

probe_block: if GENERATE_ILA = true generate
  
  fabric_ila : ila_0
	PORT MAP (
		clk		=> clk,
		probe0	=> probes
	);

	probes(SEL_WIDTH - 1 downto 0) <= sel;
	probes(6) <= qstrobe;
	probes(7) <= ored_ack(0);
	probes(39 downto 8) <= ipb_in.ipb_addr;

	probegen: for i in NSLV-1 downto 0 generate
	begin
		probes(i + 40) <= ipb_from_slaves(i).ipb_ack;
	end generate probegen;

  end generate probe_block;

  end rtl;
