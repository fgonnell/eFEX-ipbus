#!/bin/env python
'''
Create a board buffer data file.
'''

import logging
import ansistrm

import mp7tools.helpers as hlp
import mp7tools.buffers as buffers
import mp7tools.dataio as dataio
import sys
import argparse


def findPackets( buf ):
    ranges = []

    v = False
    begin = end = None

    for i,x in enumerate(buf):
        if not v:
            if (x >> 32):
                v = True
                begin = i
            continue
        # print v,x
        else:
            if not (x >> 32):
                v = False
                end = i-1
                ranges.append( (begin,end) )
            continue
    if v and (begin is not None):
        end = len(buf)-1
        ranges.append( (begin,end) )
    return ranges


# for buf in etaplus:
    # print findPackets(buf)

def decode( half ):

    tET=0
    tHT=10
    tJ0=36
    tJ1=37
    # tJ0=37
    # tJ1=38
    allPackets = set(map(tuple,[findPackets(buf) for buf in half]))

    if len(allPackets) != 1:
        logging.error('Format error? Packets not aligned in eta+?')
        sys.exit(-1)

    stripvalid = (1<<32)-1
    mask6b = (1<<6)-1
    mask7b = (1<<7)-1

    packets = allPackets.pop()
    logging.info('%d packet(s) found', len(packets))
    for i,(b,e) in enumerate(packets):

        logging.debug('Packet %d: [%d,%d] len:%d frames' ,i,b,e,e-b+1)
        if (e-b+1) <= tJ1:
            logging.error('The packet is toot short, last jets are expected at %d', tJ1)
            continue

        payload = [ v[b:e+1] for v in half ]
        ET = payload[0][tET] & stripvalid 
        MET_x = payload[1][tET] & stripvalid
        MET_y = payload[2][tET] & stripvalid

        logging.info('ET=%d, MET(x)=%d, MET(y)=%d', ET, MET_x, MET_y)

        HT = payload[0][tHT] & stripvalid 
        MHT_x = payload[1][tHT] & stripvalid
        MHT_y = payload[2][tHT] & stripvalid

        logging.info('HT=%d, MHT(x)=%d, MHT(y)=%d', HT, MHT_x, MHT_y)

        # gather jets
        jets = [ payload[k][tJ0] & stripvalid for k in xrange(3) ] + [ payload[k][tJ1] & stripvalid for k in xrange(3) ]
        # print jet
        logging.info('%d jet(s) found', sum( j > 0 for j in jets))
        for i,j in enumerate(jets):
            if j == 0: break

            # print bin(j)
            eta=j & mask6b
            phi=(j>>6) & mask7b
            energy=(j>>13)

            logging.info('Jet%d: energy=%d, phi=%d, eta=%d', i,energy, phi, eta)


def decode20uintto32int(x):
    import ctypes
    y = ctypes.c_uint32(x << 12)
    return types.cast(ctypes.pointer(y),ctypes.POINTER(ctypes.c_int)).contents.value >> 12

# logging initialization
hlp.initLogging( logging.DEBUG )

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('mpoutput')
args = parser.parse_args()

logging.info('Analising %s', args.mpoutput)
datalist = dataio.readValid( args.mpoutput )

demux = datalist[0]

etaplus  = [ demux.getlinkbuffer(i) for i in xrange(3) ]
etaminus = [ demux.getlinkbuffer(i) for i in xrange(3,6) ]

logging.info('Positive eta')
decode(etaplus)
logging.info('Negative eta')
decode(etaminus)


