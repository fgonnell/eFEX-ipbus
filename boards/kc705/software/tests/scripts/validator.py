#!/bin/env python

import logging
import ansistrm
import subprocess
import os.path
import sys

import mp7tools.helpers as hlp

def measurelatency( path ):

    import mp7tools.dataio as dataio
    datalist = dataio.readValid( path )

    data = datalist[0]

    allPackets = set(map(tuple,[hlp.findPackets(buf) for l,buf in data.iterlinks()]))

    if len(allPackets) != 1:
        logging.error('Something went wrong. Multiple packets found')
        return -1

    packets = allPackets.pop()
    logging.notice('> Measured latency: %d', packets[0][0])

    return 0

# logging initialization
hlp.initLogging( logging.DEBUG )

args = { 'board':'MP7_CERN_MAC93_CH' }
vers  = ['r1','xe']
tests = {
    # '0.0' : {},
    '1.01': {
    'versions' : vers,
    'commands' : ['reset {board} --clksrc=internal',],
    'stoponerror' : True
    },
    '1.02': {
    'versions' : ['xe'],
    'commands' : ['reset {board} --clksrc=internal --clkOpt=si570']
    },
    '1.03': {
    'versions' : ['xe'],
    'commands' : ['reset {board} --clksrc=internal --clkOpt=si5326']
    },
    '1.04': {
    'versions' : vers,
    'commands' :['mgts {board} --loopback']
    },
    '1.05': {
    'versions' : vers,
    'commands' : ['mgts {board} --loopback --orbittag']
    },
    '1.06': {
    'versions' : vers,
    'commands' : ['clocks {board}']
    },
    '1.08':{
        'versions' : vers,
        'commands' : [
            'reset {board} --clksrc=internal',
            'mgts {board} --loopback',
            'buffers {board} loopPatt',
            'capture {board} --path=loopPatt'
            ],
        'post' : [ ( measurelatency, { 'path':'loopPatt/rx_summary.txt' } ) ]
    },
    '1.09':{
        'versions' : vers,
        'commands' : [
            'reset {board} --clksrc=internal',
            'buffers {board} algoPatt',
            'capture {board} --path=algoPatt'
            ],
        'post' : [ ( measurelatency, { 'path':'algoPatt/tx_summary.txt' } ) ]
    },
    '1.10':{
        'versions' : vers,
        'commands' : [
            'reset {board} --clksrc=internal',
            'mgts {board} --loopback',
            'buffers {board} loopPlay --inject=generate:pattern',
            'capture {board} --path=loopPlay'],
        'post' : [ ( measurelatency, { 'path':'loopPlay/rx_summary.txt' } ) 
        ]
    },
    '1.11':{
        'versions' : vers,
        'commands' : [
            'reset {board} --clksrc=internal',
            'buffers {board} algoPlay --inject=generate:pattern',
            'capture {board} --path=algoPlay'],
        'post' : [ ( measurelatency, { 'path':'algoPlay/tx_summary.txt' } ) 
        ]
    },
    '1.12':{
        'versions' : vers,
        'commands' : ['scansd {board}']
    },
    '1.13':{
        'versions' : vers,
        'commands' : ['downloadimage {board} --fwpath=. --fwfile=GoldenImage.bin']
    },
    '1.14':{
        'versions' : vers,
        # to be completed
        'commands' : ['uploadfw {board} --fwpath=./GoldenImage.bin --fwfile=TestUpload.bin','scansd {board}']
    },
    '1.15':{
        'versions' : vers,
        # to be completed
        'commands' : ['deleteimage {board} --delfile=TestUpload.bin','scansd {board}']
    },
    '1.17': {
        'versions' : vers,
        'commands' : ['minipods {board}']
    },
    # 2.X set
    '2.01': {
        'versions' : vers,
        'commands' : ['reset {board} --clksrc=external']
    },
    '2.02': {
        'versions' : vers,
        'commands' : ['checkttc {board}']
    },
    '2.03': {
        'versions' : vers,
        'commands' : ['ttcscan {board}']
    },
}



def check( tid,test ):
    '''Makes sure test has all the right pieces
    '''
    if not 'versions' in test:
        logging.critical('No versions defined for test %s?!?', tid)
        sys.exit(-1)

    if not 'commands' in test:
        logging.critical('No commands defined for test %s?!?', tid)
        sys.exit(-1)

def main():
    import sys

    butler = os.path.expandvars('${MP7_TESTS}/scripts/mp7butler.py' )
    
    using = 'r1'
    testdir = 'tests'
    logflag = '--log step_{sid}.log'


    cwd = os.getcwd()

    if not os.path.exists(testdir):
        os.makedirs(testdir)

    logging.debug('Switching to %s',testdir)
    os.chdir(testdir)

    # store testdir as cwd
    testdir = os.getcwd()
    
    activeids = sys.argv[1:] if 'all' not in sys.argv[1:] else tests.iterkeys()
    activetests = dict( [ (tid,test) for tid,test in tests.iteritems() if tid in activeids ] )

    report = {}
    for tid in sorted(activetests.iterkeys()):
        logging.notice('> Starting %s', tid)
        test=tests[tid]
        check(tid,test)

        # print test['versions']
        if using not in test['versions']:
            logging.warn('Skipping test %s for version %s', tid, using)
            continue

        targs = args.copy()
        targs['tid'] = tid

        # go to a dedicated directory
        logging.debug('Jumping in %s',tid)
        if not os.path.exists(tid):
            os.makedirs(tid)
        os.chdir(tid)
        
        # continue
        success = True
        for i,cmd in enumerate(test['commands']):
            
            sargs = targs.copy()
            # step id
            sargs['sid'] = i

            cmd = (logflag+' '+cmd).format(**sargs)
            logging.notice('> Starting test %s (step %d): %s ', tid,i,cmd)
            code = subprocess.call([butler,'-q']+cmd.split())

            (logging.notice if code == 0 else logging.error)('> Test %s step %s return code: %s',tid,i,code)

            success = (success and code == 0)

        if not success:
            logging.error('Test %s failed', tid)

        if 'post' in test:
            for f,a in test['post']:
                success = (success and f(**a) == 0)

        report[tid] = success

        logging.debug('Back to %s',testdir)
        os.chdir(testdir)

    logging.notice('> Summary')
    statuses = ['Failed','Ok']
    for tid in sorted(report):
        logging.notice('%s : %s', tid,statuses[report[tid]])


if __name__ == '__main__':
    main()