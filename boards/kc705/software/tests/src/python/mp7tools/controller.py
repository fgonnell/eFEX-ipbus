
import time
import os
import mp7

import buffers
import dataio
import logging
import helpers

# from __future__ import print_function

#---
class LinkErrorException(StandardError):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

#---
class RegionGroup:
    def __init__(self, regions):

        self._avaliableChannels = sorted( set( [ c for c in xrange( 4*(max(regions)+1) ) if c/4 in regions ] ) ) if len(regions) > 0 else []
        self._availableRegions  = regions
        self._enabledChannels = []
        self._enabledRegions = []
        self._enabledRegion2Channels = {}

        self.enable(self._avaliableChannels)


    def __str__(self):
        return ('regions='+helpers.intlist2str(self._enabledRegions)+'/'+helpers.intlist2str(self._availableRegions)+
            ' channels='+helpers.intlist2str(self._enabledChannels)+'/'+helpers.intlist2str(self._avaliableChannels))

    def availableChannels(self):
        return self._avaliableChannels

    def availableRegions(self):
        return self._availableRegions

    @property
    def channels(self):
        return self._enabledChannels

    @property
    def regions(self):
        return self._enabledRegions

    def channelRanges(self):
        return helpers.intlist2str(self.channels)

    def regionRanges(self):
        return helpers.intlist2str(self.regions)  

    @property
    def regions2channels(self):
        return self._enabledRegion2Channels

    def enable(self, value):
        if value is None:
            return

        if not ( set(value) <= set(self._avaliableChannels) ):
            logging.warning('The requested links are not a subset of the available links')

        self._enabledChannels = [c for c in self._avaliableChannels if c in value]
        self._enabledRegions = sorted(set( l/4 for l in self._enabledChannels ))
        self._enabledRegion2Channels = dict([(r,list()) for r in self._availableRegions ])
        
        for l in self._enabledChannels:
            self._enabledRegion2Channels[l/4].append(l)   

# Parameters
_max_quads = 18
_max_nchans = _max_quads*4

# maybe a dict
classnames = ['MP7Controller','MP7JetTester','MP7XEController']
models = {'r1':'MP7Controller', 'xe':'MP7XEController' }

class MP7Controller(object):
    _log = logging.getLogger('MP7Controller')


    def __init__(self, board, links = None ):
        '''
        Creator. If links is None, an attempt to autodiscover the active links is done.
        '''
        self.board = board

        self._buildNodes()

        self._buildRegions(links)

        # self.log.debug( 'Detected buffer regions: %s', self._bufferRegionsIds )
        # self.log.debug( 'Detected mgts : %s', self.mgts )

        self.log.debug('%s (%s) built', self.name(), self.__class__.__name__)
        self.log.debug(' - Buffers: %s', self.bufferGroup)
        self.log.debug(' - Mgts: %s', self.mgtGroup)

    def __str__(self):
        return self.__class__.__name__+'(\''+self.name()+'\')'

    __repr__ = __str__

    def _buildRegions(self, links=None):
        '''Discover the links instanciated in the firmware.
        '''
        ctrl = self.ctrl
        
        self._regionsIds = ctrl.getRegions()
        self._bufferRegionsIds = ctrl.getBufferRegions()
        self._mgtRegionsIds = ctrl.getMGTRegions()
        

        self.bufferGroup = RegionGroup(self._bufferRegionsIds)
        self.bufferGroup.enable(links)

        self.mgtGroup = RegionGroup(self._mgtRegionsIds)
        self.mgtGroup.enable(links)

    def _buildNodes(self):

        # easy access to the wrappers
        self.log.debug(' Building ctrl')
        self.ctrl     = self.board.getNode('ctrl')
        self.log.debug(' Building clocking')
        self.clocking = self.board.getNode('ctrl.clocking')
        self.log.debug(' Building si5326')
        self.si5326   = self.board.getNode('ctrl.clocking.i2c_si5326')

        self.log.debug(' Building ttc')
        self.ttc      = self.board.getNode('ttc')
        
        self.log.debug(' Building formatter')
        self.fmt      = self.board.getNode('formatter')
        self.log.debug(' Building mgts')
        self.datapath = self.board.getNode('datapath')
        self.log.debug(' Building align')
        self.mgts      = self.board.getNode('datapath.region.mgt')
        self.log.debug(' Building channel buffer')
        self.buf      = self.board.getNode('datapath.region.buffer')
        self.log.debug(' Building datapath')
        self.align    = self.board.getNode('datapath.align')
        self.log.debug(' Building mmcPipe')
        
        self.mmcPipe  = self.board.getNode('uc')

        self._generics = self.readGenerics()

    # Tester Properties
    @property
    def log(self):
        return self._log
    
    @property
    def generics(self):
        return self._generics

    
    def printId(self):
        
        idnode = self.ctrl.getNode('id')
        a = idnode.getNode('fwrev.a').read()
        b = idnode.getNode('fwrev.b').read()
        c = idnode.getNode('fwrev.c').read()
        design = idnode.getNode('fwrev.design').read()
        idnode.getClient().dispatch()
        self.log.info('Firmware review: %s %02d.%02d.%02d', hex(design), a.value(), b.value(), c.value() )

        for k in sorted(self._generics.iterkeys()):
            self.log.debug("%s : %s", k,self._generics[k])

    def readGenerics(self):
        return helpers.snapshot(self.ctrl.getNode('id.generics'))

    def name(self):
        return self.board.id()

    def dispatch(self):
        self.board.dispatch()        

    def reset(self, clkOpt, extClk40Src = True, overrideBC0=False ):
        '''
        Reset the board status and configure clocking.

        '''
        # Meaningful configurations
        # - internal clock can be only 250
        # - SI5326 configuration is needed for 160/240 only
        ctrl    = self.ctrl
        ttc     = self.ttc

        # let's make life easier
        ttcEnable     = extClk40Src
        bc0Internal   = overrideBC0 if overrideBC0 is not None else (not extClk40Src)

        self.log.warn('Resetting board %s',self.name())
        
        # ctrl.nuke()
        ctrl.softReset()
        
        self.log.info('Reset done')
        
        self.configureClocking(clkOpt, extClk40Src)

        self.configureTTC(ttcEnable, bc0Internal)

        #---
        self.log.info('Measuring clk40 frequency. Give me a second...')
        # Check the clock 40 frequency first
        # 1.1 sec is the minimum to guarantee the freq measurement to be completed, always
        try:
            logging.info('   Clock 40 frequency: %.3f Mhz',ttc.freqClk40(1.1))
        except mp7.TTCFrequencyInvalid as e:
            self.log.error(e)

        # All good, reset algo block if there is any
        self.resetPayload()

    def resetPayload(self):
        self.log.info('Resetting algos: nothing to do')
        

    def configureClocking(self, clkOpt, extClk40Src):
        ctrl     = self.ctrl
        clocking = self.clocking
        si5326   = self.si5326

        self.log.info('Configuring clock 40')
        # leave clk40 in reset until the clocking is set
        ctrl.getNode('csr.ctrl.clk40_rst').write(0x1)
        ctrl.getNode('csr.ctrl.clk40_sel').write(extClk40Src)

#        #-- clocking configuration is pure routing
#        self.log.info('Configuring Xpoint')
        
        # configure ExternalAMC13 clock40 and
        Clk40Select = mp7.ClockingNode.Clk40Select
        RefClkSelect = mp7.ClockingNode.RefClkSelect
        
#        if not extClk40Src:
#            clocking.configureXpoint(Clk40Select.Disconnected, RefClkSelect.Oscillator)
#        else:
#            clocking.configureXpoint(Clk40Select.ExternalAMC13, RefClkSelect.Oscillator)
#
#            # Configure the si5326 clock only if in external mode
#            self.log.info('Configuring SI5326')
#            sicfg = '${MP7_TESTS}/etc/mp7/sicfg/si5326/MP7_SI5326_20130606_40.1MHz_CKIN1_to_160.4MHz_CKOUT1_NoIncDec_Regs.txt'
#            self.log.info('   Using %s', sicfg)
#            
#            # reset the chip
#            clocking.si5326Reset()
#            
#            # sleep
#            time.sleep(1)
#            
#            # and then reconfigure
#            si5326.configure(os.path.expandvars(sicfg))
#            
#            time.sleep(0.5)
#            
#            # Wait the si5326 to recover
#            try:
#                clocking.si5326WaitConfigured()
#            except mp7.SI5326ConfigurationTimeout as e:
#                self.log.error(e)
#            
#            si5326.debug()
        # All steps completed, release clock 40
        ctrl.getNode('csr.ctrl.clk40_rst').write(0x0)
        ctrl.getClient().dispatch()
        
        ctrl.waitClk40Lock()


    def configureTTC(self, ttcEnable, bc0Internal):
        ttc  = self.ttc
        ctrl = self.ctrl
        
        self.log.info('Configuring ttc')

        # enable ttc L1As and Bgos commands
        ttc.getNode('csr.ctrl.ttc_enable').write(ttcEnable)
        ttc.getClient().dispatch();
        
        # clear counters
        ttc.clear()
        
        ttc.getNode('csr.ctrl.int_bc0_enable').write(bc0Internal)
        ttc.getClient().dispatch()

        # wait for the BC0 lock only for external input
        try:
            if ttcEnable: ttc.waitBC0Lock()
        except mp7.BC0LockFailed as e:
            self.log.error(e)

        # some additional checks
        self.log.info('Post-reset stats')
        self.checkTTC()

    def checkTTC(self):
        ttc  = self.ttc
        ctrl = self.ctrl

        csrdump =  mp7.snapshot(ctrl.getNode('csr'))
        ttcdump =  ttc.snapshot()
        try:
            f = ttc.freqClk40(1.1)
        except mp7.TTCFrequencyInvalid as e:
            self.log.error(e)
            f = 0.

        self.log.info('TTC Stats')
        self.log.info('   Measured f40 : %.3f MHz' , f)
        # self.log.info('   BC0 reg      : %04x'     , ttcdump['csr.stat0'] >> 16)
        self.log.info('   BC0 Internal : %d '      , ttcdump['csr.ctrl.int_bc0_enable'] )
        self.log.info('   BC0 Lock     : %d '      , ttcdump['csr.stat0.bc0_lock'] )
        self.log.info('   BC0 Error    : %d '      , ttcdump['csr.stat0.bc0_err'] )
        self.log.info('   Dist lock    : %d '      , ttcdump['csr.stat0.dist_lock'] )

        v  = csrdump['stat']
        s0 = ttcdump['csr.stat0']
        s1 = ttcdump['csr.stat1']
        s2 = ttcdump['csr.stat2']
        s3 = ttcdump['csr.stat3']
        self.log.info('   Status: %08x, Bunch: %x, Evt: %x, Orb %x', int(v), ttcdump['csr.stat0.bunch_ctr'], ttcdump['csr.stat1.evt_ctr'], ttcdump['csr.stat2.orb_ctr'] )
        
        self.log.info('Errors')
        self.log.info('   Single Bit Errors: %d', ttcdump['csr.stat3.single_biterr_ctr'])
        self.log.info('   Double Bit Errors: %d', ttcdump['csr.stat3.double_biterr_ctr'])


        self.log.debug('----Breakdown')
        for k,v in sorted(ttcdump.iteritems(), key=lambda x: x[0]):
            self.log.debug('%s : %d',k,v)
        self.log.debug('------------')

    #---
    def scanTTCPhase(self, start=0x0, stop=0x540):
        import time
        ttc  = self.ttc
        ctrl = self.ctrl

        self.log.warn('Resetting clock 40 and TTC phase settings')

        # Clock 40 reset to reset the TTC phase setting before the scan. 
        ctrl.getNode('csr.ctrl.clk40_rst').write(0x1)
        ctrl.getClient().dispatch()
        time.sleep(0.1)        
        ctrl.getNode('csr.ctrl.clk40_rst').write(0x0)
        ctrl.getClient().dispatch()

        ctrl.waitClk40Lock()

        self.log.info('TTC phase scan range: [0x%x,0x%x]', start,stop)
        ttc.getNode('csr.ctrl1.ttc_phase_en').write(1)
        ttc.getClient().dispatch()

        goodphases = []

        for i in xrange(start,stop):
            ttc.getNode('csr.ctrl1.ttc_phase').write(i)
            ttc.getClient().dispatch()
            phase_ok = ttc.getNode('csr.stat0.ttc_phase_ok').read()
            ttc.getClient().dispatch()
            if int(phase_ok) != 1:
                self.log.warn('Phase not locked %s', hex(phase_ok))

            ttc.getNode('csr.ctrl.err_ctr_clear').write(1)
            ttc.getNode('csr.ctrl.err_ctr_clear').write(0)
            ttc.getClient().dispatch()
            time.sleep(0.01)
            errs = ttc.getNode('csr.stat3').read()
            ttc.getClient().dispatch()
            self.log.debug('phase %d: error counts: %s', i, hex(errs))
            if int(errs) == 0:
                goodphases.append(i)

        ttc.getNode('csr.ctrl1.ttc_phase_en').write(0)
        ttc.getClient().dispatch()

        self.log.info( 'Good TTC phases are %s/[%d,%d]',helpers.intlist2str(goodphases), start, stop)
        # self.log.debug('                hex %s/[%sz,%d]',helpers.intlist2str(goodphases), start, stop)

    #---
    def setupTx2RxPattern( self ):
        # Internal buffer loop :  Tx -> Rx
        # Uses playbacks
        '''
        Configures the MP7 buffers for Tx->Rx pattern 
        '''

        from buffers import PathConfigurator

        ctrl  = self.ctrl
        buf   = self.buf

        rxconfig = PathConfigurator('capture',0xdec-1,1023)

        # pattern for 10 bx
        pattrng = (0x0,0x10)
        self.log.info('Configuring links in loopback. Patterns over bx range %s', pattrng)
        txconfig = PathConfigurator.withBXRange('pattern',pattrng,**(self.generics))

        # print 'rxconfig',rxconfig
        # print 'txconfig',txconfig

        for l in self.mgtGroup.channels:

            ctrl.selectLinkBuffer(l,ctrl.BufferSelect.Tx)

            txconfig.configure(buf)

            ctrl.selectLinkBuffer(l,ctrl.BufferSelect.Rx)

            rxconfig.configure(buf)

        self.log.warn('Generating Tx -> Rx patterns')

 
    #--- 
    def setupTx2RxOrbitPattern( self ): 
        # Internal buffer loop :  Tx -> Rx 
        # Uses playbacks 
        ''' 
        Configures the MP7 buffers for Tx->Rx pattern  
        ''' 

        from buffers import PathConfigurator 

        ctrl  = self.ctrl 
        buf   = self.buf 

        # 1024 frames satrting at bx 0
        pattrng = (0xdec-1,0x400-1) 

        self.log.warn('Using orbit alignment pattern') 
        rxconfig = PathConfigurator('capture',*pattrng) 


        self.log.info('Configuring links in loopback. Patterns over bx range %s', pattrng) 
        txconfig = PathConfigurator('playonce',*pattrng) 

        ds = buffers.BoardDataSource('generate:orbitpattern') 

        self.clearBuffers( data_tx=ds.data, forceclear=True ) 


        for l in self.mgtGroup.channels: 

            ctrl.selectLinkBuffer(l,ctrl.BufferSelect.Tx) 

            txconfig.configure(buf) 

            # print mp7.snapshot(ctrl.getNode('csr')) 

            ctrl.selectLinkBuffer(l,ctrl.BufferSelect.Rx) 

            rxconfig.configure(buf) 

        self.log.warn('Generating Tx -> Rx orbit patterns') 	 

    #---
    def enableChanAlignment(self):
        '''
        Enable alignment for channels belonging to the active quads.
        '''

        # need to provide 72 values
        activechans = [ (c in self.mgtGroup.channels) for c in xrange(_max_nchans) ]

        self.log.debug('Enabling channels: %s ' % ''.join([('1' if c else '0') for c in activechans]))

        self.align.enable( activechans )

    def clearCounters(self):
        ctrl = self.ctrl
        mgts = self.mgts
        
        self.log.info('Clearing counters')
        for r in self.mgtGroup.regions:
            ctrl.selectRegion(r)
            mgts.clear()


    #---
    def configureLinks( self, loopback=False ):
        '''
        Configure and align MGTs.
        '''

        # Local references
        mgts   = self.mgts
        ctrl   = self.ctrl
        align = self.align

        loop = 2 if loopback else 0


        self.log.info('Configuring Quads %s', self.mgtGroup.regions)
        for r in self.mgtGroup.regions:
            self.log.debug('--> Q%02d', r)
            ctrl.selectRegion(r)
            mgts.configure(loop,loop,loop,loop)

            # mgts.loopback(loop,loop,loop,loop)
            # mgts.softReset()
            # mgts.
                        
        self.log.info('Done')
        self.clearCounters()

        self.log.info('Checking MGTs for errors')
        for r in self.mgtGroup.regions:
            ctrl.selectRegion(r)
            
            links = self.mgtGroup.regions2channels[r]
            errs = [mgts.checkQuad()]+[mgts.checkChannel(l%4) for l in links]
            err = any(errs)
            qlog = self.log.error if err else self.log.debug
            qlog('--> Q%02d: %s',r,('Ok','Error')[err])

        clk40_locked = ctrl.clock40Locked()
        self.log.info('Clock 40 is %s',('unlocked','locked')[clk40_locked])


    def alignLinks(self, secondary=False, orbitTag=False, fixlatency=None ):
        '''

        '''

        # Local references
        mgts  = self.mgts
        ctrl  = self.ctrl
        align = self.align


        orbit_refgs = [ 'rw_regs.ch%d.control.orbit_tag_enable' % i for i in xrange(4) ] 
 
        for r in self.mgtGroup.regions: 
            ctrl.selectRegion(r) 
            if orbitTag: 
                for r in orbit_refgs: 
                    mgts.getNode(r).write(0x1) 
            else: 
                try: 
                    for r in orbit_refgs: 
                        mgts.getNode(r).write(0x0) 
                except mp7.exception as e: 
                    self.log.error(e) 

            mgts.getClient().dispatch() 
            

        # time.sleep(1)

        if secondary:
            self.log.warn('Using secondary alignment master')
        align.getNode('ctrl.secondary_master').write(secondary)
        # magic parameter? What is it for?
        align.getNode('ctrl.margin').write(3)
        if fixlatency is None:
            align.getNode('ctrl_r2.fixed_latency').write(0x0)
        else:
            clock_ratio = self.generics['clock_ratio']
            bctr = fixlatency / clock_ratio
            sctr = fixlatency % clock_ratio
            print bctr,sctr
            align.getNode('ctrl_r2.bunch_ctr_req').write(bctr)
            align.getNode('ctrl_r2.sub_bunch_ctr_req').write(sctr)
            align.getNode('ctrl_r2.fixed_latency').write(0x1)
        align.getClient().dispatch()


        # set the align mask for the unused quads
        self.enableChanAlignment()

        try:
            self.log.info('Running alignment algorithm')
            # perform alignment
            align.align()

        except RuntimeError as e:
            self.log.error('Failed aligning channels')


        # Report on alignment status
        v = align.getNode('stat').read()
        self.board.dispatch()
        self.log.info( 'Summary:')
        self.log.info( '-> Status: %x', v)

        aligndump = align.snapshot('stat\..*')
        # get the clock ratio again. FIXME this is not going to change, what about doing it once for all?
        clk_ratio = self.generics['clock_ratio']
        min_bunch_ctr = aligndump['stat.min_bunch_ctr']
        min_sub_bunch_ctr = aligndump['stat.min_sub_bunch_ctr']
        min_clks = min_bunch_ctr*clk_ratio+min_sub_bunch_ctr
        # min_clks = float(min_bunch_ctr+min_sub_bunch_ctr)/clk_ratio

        self.log.info('-> Measured minimum latency: clocks %d (bx: %s, sub_bx: %s)', min_clks, min_bunch_ctr, min_sub_bunch_ctr)
        if aligndump['stat.align_ok']:
            self.log.info('Alignment succesful')
            if fixlatency:
                self.log.info('-> Latency locked to: clocks %d (bx: %s, sub_bx: %s)', fixlatency, bctr, sctr)

        self.log.debug('----Breakdown')
        for k,v in sorted(aligndump.iteritems(), key=lambda x: x[0]):
            self.log.debug('%s : %d',k,v)
        self.log.debug('------------')

        # Clear before leaving
        self.clearCounters()

    def measureRefClocks(self):
        ttc  = self.ttc
        ctrl = self.ctrl
        
        self.log.info('Measuring clocks')
        for l in self.mgtGroup.channels:
            ctrl.selectLink(l)
            self.log.info('-> Channel %02d',l)
            self.log.info('   RefClk = %.3f MHz',ttc.measureClockFreq(mp7.TTCNode.RefClock, 1.1) )
            self.log.info('   RxClk  = %.3f MHz',ttc.measureClockFreq(mp7.TTCNode.RxClock,  1.1) )
            self.log.info('   TxClk  = %.3f MHz',ttc.measureClockFreq(mp7.TTCNode.TxClock,  1.1) )

    #---
    def checkLinks(self):
        '''
        Test all the MGTs for CRC and alignment errors.
        '''

        # Local references
        ctrl  = self.ctrl
        ttc   = self.ttc
        mgts  = self.mgts
        align = self.align

        self.log.info( 'Checking alignment' )
        # FIXME: Returning false when there are no error might _not_ be smart
        alignerr = align.check()

        aligndump = align.snapshot('stat.*')

        aligned_links = (
            (aligndump['stat_r3.aligned_links_ch71_ch64']<<64)+
            (aligndump['stat_r2.aligned_links_ch63_ch32']<<32)+
            aligndump['stat_r1.aligned_links_ch31_ch00']
            )

        if alignerr:
            aligndump = align.snapshot('stat.*')
            self.log.warn('--Alignment: Error')
            self.log.warn('--Misaligned links: %s', [l for l in self.mgtGroup.channels if (((aligned_links >> l) & 1) == 0)])
            self.log.warn('----Registers breakdown')
            for r in sorted(aligndump.iterkeys()):
                self.log.warn('%s : %s',r,hex(aligndump[r]))
            self.log.warn('------------------')
        else:
            self.log.debug( '--Alignment: OK')

        self.log.info( 'Checking CRCs')
        crcerrs = []

        for r in self.mgtGroup.regions:
            links = self.mgtGroup.regions2channels[r]

            qerr = mgts.checkQuad()
            # qerr = True;
            if qerr:
                self.log.error('Errors detected on quad %d',r)
                self.log.warn('-- Dumping quad %s registers',r)
                regdump = mgts.snapshot('ro_regs.common\..*')
                for r in sorted(regdump.iterkeys()):
                    self.log.warn( '     %s:%x', r, regdump[r])

            lerrs = []
            for l in links:
                c = l%4
                ctrl.selectLink(l)
                if self.mgts.checkChannel( c ):
                    lerrs.append(l)
                # lerrs.append(l)
            
            if lerrs:
                self.log.error('Errors detected on links %s (r%d)',','.join([str(e) for e in  lerrs]),r)
                # dump common stuff

                # dump channels
                for l in lerrs:
                    c = l%4
                    self.log.warn('-- Dumping link %s registers',l)
                    regdump = mgts.snapshot('ro_regs\.ch%d\..*' % c)
                    for r in sorted(regdump.iterkeys()):
                        self.log.warn( '     %s:%x', r, regdump[r])
                    crcerrs.append(l)


        self.log.debug( 'CRCs summary: %s',str(crcerrs))

        if alignerr or crcerrs:
            # additional measurement of clock frequency
            raise LinkErrorException('Alignment: '+('Ok','Error')[alignerr]+', CRCs on quads: '+str(crcerrs))
        else:
            self.log.info('Alignment & CRCs are OK for all channels')

    #---
    def configureHdrFormatter(self, strip, insert):
        fmt = self.fmt

        if strip is not None:
            fmt.getNode('ctrl.enable_strip').write(strip)
        if insert is not None:
            fmt.getNode('ctrl.enable_insert').write(insert)

        if strip is None and insert is None:
            self.log.warning('Header Formatter: Nothing to be done')
        else:
            fmt.getClient().dispatch()

        snap = mp7.snapshot(fmt)
        for k,v in snap.iteritems():
            self.log.debug("%s: %s", k,v)
        self.log.info('Header stripping: %s', ('enabled' if snap['ctrl.enable_strip'] else 'disabled') )
        self.log.info('Header insertion: %s', ('enabled' if snap['ctrl.enable_insert'] else 'disabled') )


    #---
    def readBuffers(self):

        # Local references
        ctrl = self.ctrl
        buf = self.buf

        depth = self.buf.getSize()

        # pull the captured data out of the buffers
        values = {}

        rxData = dataio.BoardData( self.name() )
        txData = dataio.BoardData( self.name() )

        self.log.info('Reading links: %s (%d words each)',self.bufferGroup.channelRanges(),depth)

        for c in self.bufferGroup.channels:
            self.log.debug('  - Reading Rx.%02d',c)
            ctrl.selectLinkBuffer( c, ctrl.BufferSelect.Rx )
            vRx = buf.downloadValid( depth )
            rxData.addlink( c, vRx )

            self.log.debug('  - Reading Tx.%02d',c)
            ctrl.selectLinkBuffer( c, ctrl.BufferSelect.Tx )
            vTx = buf.downloadValid( depth )
            txData.addlink( c, vTx )

        return rxData,txData

    #---
    def clearBuffers( self, data_rx = None, data_tx = None, forceclear=False ):
        '''Clears the Rx/Tx buffers

        Args:
            data_rx:
            data_tx:
        '''

        # Local references
        ctrl = self.ctrl
        buf = self.buf

        # print data_rx, data_tx
        self.log.info( 'Clearing Buffers (force=%s)','Yes' if forceclear else 'No')
        for link in self.bufferGroup.channels:

            ctrl.selectLinkBuffer( link, ctrl.BufferSelect.Rx )
            # print buf.getBufferMode()

            if data_rx:
                # If pattern upload is requested, clear and upload
                self.log.debug('Clearing & loading Rx.%02d', link)
                buf.clear()
                buf.uploadValid( data_rx.getlinkbuffer(link) )
            elif buf.getBufferMode() == buf.Capture or forceclear:
                # Clear buffer only if in capture mode or if forced
                self.log.debug('Clearing Rx.%02d', link)
                buf.clear()

            # self.log.debug('Clearing %02d Tx', link)
            ctrl.selectLinkBuffer( link, ctrl.BufferSelect.Tx )
            # print buf.getBufferMode()


            if data_tx:
                # If pattern upload is requested, clear and upload
                self.log.debug('Clearing & loading Tx.%02d', link)
                buf.clear()
                buf.uploadValid( data_tx.getlinkbuffer(link) )
            elif buf.getBufferMode() == buf.Capture or forceclear:
                # Clear buffer only if in capture mode or if forced
                self.log.debug('Clearing Tx.%02d', link)
                buf.clear()

    #---
    def captureAndDump(self, path, data_rx=None, data_tx=None, capture=True ):
        '''
        if cap_depth is 0x0 or none, don't capture
        '''
        # Local references
        ctrl = self.ctrl
        ttc = self.ttc
        buf = self.buf

        # print mp7.snapshot(buf.getNode('csr.stat'))
        # if forceclear:
        # Clear buffers in a smart way
        self.clearBuffers( data_rx, data_tx  )

        if capture:
            self.log.info('Capturing buffers')
            
            # Capture the buffers
            ttc.sendBTest()
            # And check it's done
            # time.sleep(1)
            buf.waitCaptureDone()
            self.log.info('Capture completed')

        rxData,txData = self.readBuffers()

        # Dump
        if path:

            os.system('mkdir -p '+path)

            dataio.writeValid(rxData, os.path.join(path,'rx_summary.txt'))
            dataio.writeValid(txData, os.path.join(path,'tx_summary.txt'))

        return rxData, txData

    def dump( self, path ):
        rxData,txData = self.readBuffers()

        os.system('mkdir -p '+path)

        dataio.writeValid(rxData, os.path.join(path,'rx_summary.txt'))
        dataio.writeValid(txData, os.path.join(path,'tx_summary.txt'))

        return rxData, txData

    def minipodSensors(self):

        #Local references
        ctrl = self.ctrl

        #Get minipod nodes & print sensor info
#        for name in ctrl.getNodes("minipods_.*\..x."):
#            mpNode = ctrl.getNode(name)
#            self.log.info("Reading MiniPOD %s", mpNode.getId())
#            self.log.info("%s 3v3            = %s", mpNode.getId(), mpNode.get3v3())
#            self.log.info("%s 2v5            = %s", mpNode.getId(), mpNode.get2v5())
#            self.log.info("%s Temp           = %s", mpNode.getId(), mpNode.getTemp())
#            self.log.info("%s on time        = %s", mpNode.getId(), mpNode.getOnTime())
#            self.log.info("%s optical powers = %s", mpNode.getId(), mpNode.get3v3())
        
        minipods = [ ctrl.getNode(m) for m in ['minipods_top','minipods_bot']]
        
        for node in minipods:
            for pod in sorted(node.getRxPODs()):
                rxpod = node.getRxPOD(pod)
                self.log.info("Reading MiniPOD %s", pod) #rxpod.getId())
                self.log.info("%s 3v3            = %s", pod, rxpod.get3v3())
                self.log.info("%s 2v5            = %s", pod, rxpod.get2v5())
                self.log.info("%s Temp           = %s", pod, rxpod.getTemp())
                self.log.info("%s on time        = %s", pod, rxpod.getOnTime())
                self.log.info("%s optical powers = %s", pod, rxpod.get3v3())
            for pod in sorted(node.getTxPODs()):
                rxpod = node.getTxPOD(pod)
                self.log.info("Reading MiniPOD %s", pod) #rxpod.getId())
                self.log.info("%s 3v3            = %s", pod, rxpod.get3v3())
                self.log.info("%s 2v5            = %s", pod, rxpod.get2v5())
                self.log.info("%s Temp           = %s", pod, rxpod.getTemp())
                self.log.info("%s on time        = %s", pod, rxpod.getOnTime())
                self.log.info("%s optical powers = %s", pod, rxpod.get3v3())
           
    def scanSD(self):

        self.log.info("Listing files on uSD...")
        mmc  = self.mmcPipe
        for filename in mmc.ListFilesOnSD():
            print(filename)
        
    def setDummySensor(self, value):
        mmc = self.mmcPipe
        mmc.SetDummySensor(value)

    def fileToSD(self, filepath, filename):
        mmc = self.mmcPipe
        if 'bin' in filename:
            firmware = mp7.XilinxBinFile(filepath)
        elif 'bit' in filename:
            firmware = mp7.XilinxBitFile(filepath)
        mmc.FileToSD(filename, firmware)
        self.scanSD()

    def rebootFPGA(self, filename):
        mmc = self.mmcPipe
        mmc.RebootFPGA(filename, "RuleBritannia")
        time.sleep(3)
        textSpace = mmc.GetTextSpace()
        if textSpace == filename:
            logging.info("FPGA has rebooted with firmware image %s" % filename)
        else:
            logging.warning("FPGA failed to boot with image %s. Rebooted with GoldenImage.bin" % filename)
        
    def hardReset(self):
        mmc = self.mmcPipe
        mmc.BoardHardReset("RuleBritannia")

    def deleteFromSD(self, filename):
        mmc = self.mmcPipe
        mmc.DeleteFromSD(filename, "RuleBritannia")
        self.scanSD()

    def fileFromSD(self, filepath, filename):
	from os.path import join
        mmc = self.mmcPipe
        firmware = mmc.FileFromSD(filename)
        logging.info('Creating local file. Please wait a minute...')
        if not os.path.isdir(filepath):
            logging.info('Path does not yet exist. Creating dir %s...' % filepath)
            os.system('mkdir -p '+path)
        ofile = open(join(filepath,filename),'wb')
        fwByteArray = bytearray(firmware.Bitstream())
        ofile.write(fwByteArray)
        ofile.close()
        logging.info('Download finished. File is at: %s%s', filepath, filename)

class MP7JetTester(MP7Controller):
    _log = logging.getLogger('MP7JetTester')

    def __init__(self,*args,**kwargs):
        super(MP7JetTester,self).__init__(*args,**kwargs)

        self.payload = self.board.getNode('payload')


    def resetPayload(self):
        logging.warn('Resetting jets payload block')
        ar = self.payload.getNode('csr.ctrl.algo_reset')
        ar.write(0x1)
        ar.write(0x0)
        self.dispatch()

class MP7XEController(MP7Controller):
    _log = logging.getLogger('MP7XEController')

    def __init__(self,*args,**kwargs):
        super(MP7XEController,self).__init__(*args,**kwargs)

    def _buildNodes(self):
        # easy access to the wrappers
        self.log.debug(' Building ctrl')
        self.ctrl     = self.board.getNode('ctrl')
        self.log.debug(' Building clocking')
        self.clkcsr   = self.board.getNode('ctrl.clocking.csr')
        self.log.debug(' Building si5326 top')
        self.si570bot    = self.board.getNode('ctrl.clocking.i2c_si570_bot')
        self.log.debug(' Building si5326 top')
        self.si5326top   = self.board.getNode('ctrl.clocking.i2c_si5326_top')
        self.log.debug(' Building si5326 bottom')
        self.si5326bot   = self.board.getNode('ctrl.clocking.i2c_si5326_bot')

        self.log.debug(' Building ttc')
        self.ttc      = self.board.getNode('ttc')

        self.log.debug(' Building formatter')
        self.fmt      = self.board.getNode('formatter')
        self.log.debug(' Building mgts')
        self.datapath = self.board.getNode('datapath')
        self.log.debug(' Building align')
        self.mgts      = self.board.getNode('datapath.region.mgt')
        self.log.debug(' Building channel buffer')
        self.buf      = self.board.getNode('datapath.region.buffer')
        self.log.debug(' Building datapath')
        self.align    = self.board.getNode('datapath.align')
        self.log.debug(' Building mmcPipe')


        self.log.debug(' Building mmcPipe')
        self.mmcPipe  = self.board.getNode('uc')

        self._generics = self.readGenerics()

    def configureClocking(self, clkOpt, extClk40Src):
        ctrl      = self.ctrl
        clkcsr    = self.clkcsr
        si570bot  = self.si570bot
        si5326top = self.si5326top
        si5326bot = self.si5326bot

        self.log.info('Configuring clock 40')
        # leave clk40 in reset until the clocking is set
        ctrl.getNode('csr.ctrl.clk40_rst').write(0x1)
        ctrl.getNode('csr.ctrl.clk40_sel').write(extClk40Src)
        ctrl.getClient().dispatch();

        #-- clocking configuration is pure routing
        self.log.info('Configuring clocking')
                
        # configure ExternalAMC13 clock40 and
        Clk40Select = mp7.ClockingXENode.Clk40Select
        # RefClkSelect = mp7.clockingNode.RefClkSelect

        clkcsr.configureXpoint(Clk40Select.Disconnected if not extClk40Src else Clk40Select.ExternalAMC13)
        
        # In0 = FPGA
        # In1 = TCLK-C
        # In2 = TCLK-A        
        # In3 = FCLKA 

        # Out0 = TCLKB
        # Out1 = FPGA
        # Out2 = clk2 (Top)        
        # Out3 = clk1 (Bot) 
        # "SI53314 'BOT' clk network' Use '0' for CLK0 (SI570), '1' for CLK1 (SI5326)" />
        
        # internal clock hardcoded
        #clkcsr.configureU36(3,3,3,3)

        self.log.warn(clkOpt)
        # SI570 free run
        if clkOpt == 'si570':
            si570cfg = '${MP7_TESTS}/etc/mp7/sicfg/si570/SI570_125MHz_Regs.txt'
            si5326cfg = '${MP7_TESTS}/etc/mp7/sicfg/si5326xe/MP7_SI5326_20140605_XAXB_to_125MHz_CKOUT1_NoIncDec_Require_CKIN1_Disabled_Regs.txt'
            # deselect the top si5326 fanout targets
            clkcsr.getNode('ctrl.si53314_top_enable_base').write(0x0)
            clkcsr.getNode('ctrl.si53314_top_enable_ext').write(0x0)
            clkcsr.getNode('ctrl.si53314_top_clk_sel').write(0x0)
            # select the bottom si5326 fanout targets
            clkcsr.getNode('ctrl.si53314_bot_enable_base').write(0x1)
            clkcsr.getNode('ctrl.si53314_bot_enable_ext').write(0x1)
            clkcsr.getNode('ctrl.si53314_bot_clk_sel').write(0x0)
            clkcsr.getClient().dispatch()

        elif clkOpt == 'si5326':
            si570cfg = '${MP7_TESTS}/etc/mp7/sicfg/si570/SI570_125MHz_Regs.txt'
            if extClk40Src:     
                si5326cfg = '${MP7_TESTS}/etc/mp7/sicfg/si5326xe/MP7_SI5326_20140603_CKIN1_to_125MHz_CKOUT1_NoIncDec_Require_CKIN1_40MHz_Regs.txt'
            else:
                si5326cfg = '${MP7_TESTS}/etc/mp7/sicfg/si5326xe/MP7_SI5326_20140605_XAXB_to_125MHz_CKOUT1_NoIncDec_Require_CKIN1_Disabled_Regs.txt'
            #si5326cfg = '${MP7_TESTS}/etc/mp7/sicfg/si5326xe/MP7_SI5326_20140603_XAXB_to_125MHz_CKOUT1_NoIncDec_Require_CKIN1_Disabled_Regs.txt'
            #si5326cfg = '${MP7_TESTS}/etc/mp7/sicfg/si5326xe/MP7_SI5326_20140427_XAXB_to_150MHz_CKOUT1_NoIncDec_Require_CKIN1_Disabled_Regs.txt'
            #si5326cfg = '${MP7_TESTS}/etc/mp7/sicfg/si5326xe/MP7_SI5326_20140604_XAXB_to_125MHz_CKOUT1_NoIncDec_Require_CKIN1_Disabled_CML_Regs.txt'
            #si5326cfg = '${MP7_TESTS}/etc/mp7/sicfg/si5326xe/MP7_SI5326_20140603_CKIN1_to_125MHz_CKOUT1_NoIncDec_Require_CKIN1_40MHz_Regs.txt'
            
            # deselect the top si5326 fanout targets
            clkcsr.getNode('ctrl.si53314_top_enable_base').write(0x1)
            clkcsr.getNode('ctrl.si53314_top_enable_ext').write(0x1)
            clkcsr.getNode('ctrl.si53314_top_clk_sel').write(0x1) # 0 for SI570, 1 for SI5326
            # select the bottom si5326 fanout targets
            clkcsr.getNode('ctrl.si53314_bot_enable_base').write(0x1)
            clkcsr.getNode('ctrl.si53314_bot_enable_ext').write(0x1)
            clkcsr.getNode('ctrl.si53314_bot_clk_sel').write(0x1)
            clkcsr.getClient().dispatch()
        
        # print clkcsr.snapshot('ctrl\.rst.*')

        self.log.info('Configuring SI570 Bottom')
        self.log.info('   Using %s', si570cfg)

        # and then reconfigure
        si570bot.configure(os.path.expandvars(si570cfg))

        if clkOpt == 'si5326':
            self.log.info('Configuring SI5326')
            # Configure the si5326 clock only if in external mode
            
            self.log.info('   Using %s', si5326cfg)

            self.log.info('Configuring SI5326 Bottom')    
            # select the si5326 bottom
            clkcsr.getNode('ctrl.clk_i2c_sel').write(0x1)
            clkcsr.getClient().dispatch()

            # reset the chip
            clkcsr.si5326BottomReset()
            
            # sleep
            time.sleep(1)
            
            # and then reconfigure
            si5326bot.configure(os.path.expandvars(si5326cfg))
            
            time.sleep(0.5)
            
            bot_lol = clkcsr.getNode('stat.si5326_bot_lol').read()
            bot_int = clkcsr.getNode('stat.si5326_bot_int').read()
            clkcsr.getClient().dispatch()
            print "bot_lol=",bot_lol.value()
            print "bot_int=",bot_int.value()

            # Wait the si5326 to recover
            #clkcsr.si5326BottomWaitConfigured(False)


            si5326bot.debug()

            self.log.info('Configuring SI5326 Top')
            # select the si5326 top
            clkcsr.getNode('ctrl.clk_i2c_sel').write(0x0)
            clkcsr.getClient().dispatch()

            # # reset the chip
            clkcsr.si5326TopReset()
             
            # # sleep
            time.sleep(1)
             
            # # and then reconfigure
            si5326top.configure(os.path.expandvars(si5326cfg))
             
            time.sleep(0.5)
             
            top_lol = clkcsr.getNode('stat.si5326_top_lol').read()
            top_int = clkcsr.getNode('stat.si5326_top_int').read()
            clkcsr.getClient().dispatch()
            print "top_lol=",top_lol.value()
            print "top_int=",top_int.value()

            # # Wait the si5326 to recover
            #clkcsr.si5326TopWaitConfigured(False)
             
            si5326top.debug()



        # All steps completed, release clock 40
        ctrl.getNode('csr.ctrl.clk40_rst').write(0x0)
        ctrl.getClient().dispatch()
        ctrl.waitClk40Lock()       

    # def minipodSensors(self):

    #     #Local references
    #     ctrl = self.ctrl

    #     #Get minipod nodes & print sensor info
    #     for name in ctrl.getNodes("i2c_minipod_.*\..x."):
    #         mpNode = ctrl.getNode(name)
    #         self.log.info("Reading MiniPOD %s", mpNode.getId())
    #         self.log.info("%s 3v3            = %s", mpNode.getId(), mpNode.get3v3())
    #         self.log.info("%s 2v5            = %s", mpNode.getId(), mpNode.get2v5())
    #         self.log.info("%s Temp           = %s", mpNode.getId(), mpNode.getTemp())
    #         self.log.info("%s on time        = %s", mpNode.getId(), mpNode.getOnTime())
    #         self.log.info("%s optical powers = %s", mpNode.getId(), mpNode.get3v3())


#class MP7XEMuxTester(MP7Controller):
#    _log = logging.getLogger('MP7XEMuxTester')
#
#    def __init__(self,*args,**kwargs):
#        super(MP7XEMuxTester,self).__init__(*args,**kwargs)
#
#    def _buildNodes(self):
#        # easy access to the wrappers
#        self.log.debug(' Building ctrl')
#        self.ctrl     = self.board.getNode('ctrl')
#        self.log.debug(' Building clocking')
#        self.clocking   = self.board.getNode('ctrl.clocking')
#
#        self.log.debug(' Building SI5326 ')
#        self.si5326  = self.board.getNode('ctrl.i2c_clk')
#        self.log.debug(' Building SI570 ')
#        self.si570  = self.board.getNode('ctrl.i2c_clk')
#
#        self.log.debug(' Building ttc')
#        self.ttc      = self.board.getNode('ttc')
#        self.log.debug(' Building mgts')
#        self.mgts      = self.board.getNode('datapath.mgt')
#        self.log.debug(' Building datapath')
#        self.datapath = self.board.getNode('datapath')
#        self.log.debug(' Building align')
#        self.align    = self.board.getNode('datapath.align')
#
#        self.buf = self.datapath.manager()
#
#        self._generics = self.readGenerics()
#
#    def configureClocking(self, clkOpt, extClk40Src):
#        ctrl      = self.ctrl
#        clocking    = self.clocking
#        
#        si5326    = self.si5326
#        si570    = self.si570
#
#        self.log.info('Configuring clock 40')
#        # leave clk40 in reset until the clocking is set
#        ctrl.getNode('csr.ctrl.clk40_rst').write(0x1)
#        ctrl.getNode('csr.ctrl.clk40_sel').write(extClk40Src)
#        ctrl.getClient().dispatch();
#
#        #-- clocking configuration is pure routing
#        self.log.info('Configuring clocking')
#        
#        # internal clock hardcoded
#        clocking.configureU36(0,0,0,0)
#
#        # configure ExternalAMC13 clock40 and
#        Clk40Select = mp7.ClockingXENode.Clk40Select
#        # RefClkSelect = mp7.clockingNode.RefClkSelect
#
#
#        clocking.configure(Clk40Select.Disconnected if extClk40Src else Clk40Select.ExternalAMC13)
#
#        # select the bottom si5326 fanout targets
#        clocking.getNode('ctrl.si53314_top_enable_base').write(0x1)
#        clocking.getNode('ctrl.si53314_top_enable_ext').write(0x1)
#        
#        # select si570 as clock source of the bottom si5326
#        clocking.getNode('ctrl.si53314_top_clk_sel').write(0x0)
#        clocking.getClient().dispatch()
#
#        # select the si5326 bottom
#        clocking.getNode('ctrl.clk_i2c_sel').write(0x2)
#        clocking.getClient().dispatch()
#        
#        # print clocking.snapshot('ctrl\.rst.*')
#        if True:
#            si570cfg = '${MP7_TESTS}/etc/mp7/sicfg/si570/SI570_250MHz_Regs.txt'
#
#            # and then reconfigure
#            self.log.info('SI570')
#            si570.configure(os.path.expandvars(si570cfg))
#
#        else:
#            # Configure the si5326 clock only if in external mode
#            self.log.info('Configuring SI5326')
#            sicfg = '${MP7_TESTS}/etc/mp7/sicfg/si5326/MP7_SI5326_20130606_40.1MHz_CKIN1_to_160.4MHz_CKOUT1_NoIncDec_Regs.txt'
#            self.log.info('   Using %s', sicfg)
#        
#            self.log.info('Bottom')
#            # select the si5326 bottom
#            clocking.getNode('ctrl.clk_i2c_sel').write(0x1)
#            clocking.getClient().dispatch()
#
#            # reset the chip
#            clocking.si5326BottomReset()
#            
#            # sleep
#            time.sleep(1)
#            
#            # and then reconfigure
#            si5326.configure(os.path.expandvars(sicfg))
#            
#            time.sleep(0.5)
#            
#            # Wait the si5326 to recover
#            clocking.si5326BottomWaitConfigured()
#            
#            si5326.debug()
#
#
#            # select the si5326 top
#            clocking.getNode('ctrl.clk_i2c_sel').write(0x0)
#            clocking.getClient().dispatch()
#
#            self.log.info('Top')
#            # reset the chip
#            clocking.si5326TopReset()
#            
#            # sleep
#            time.sleep(1)
#            
#            # and then reconfigure
#            si5326.configure(os.path.expandvars(sicfg))
#            
#            time.sleep(0.5)
#            
#            # Wait the si5326 to recover
#            clocking.si5326TopWaitConfigured()
#            
#            si5326.debug()
#
#        # All steps completed, release clock 40
#        ctrl.getNode('csr.ctrl.clk40_rst').write(0x0)
#        ctrl.getClient().dispatch()
#        ctrl.waitClk40Lock()

# class PPTester(MP7Controller):

#     _log = logging.getLogger('PPTester')
#     def __init__(self, board, quads = parameters.pp.quads ):
#         super(PPTester,self).__init__(board,quads)

#         self.ppram   = mp7.PPRamNode        ( board.getNode('pp_ctrl') )

# class MPTester(MP7Controller):
#     _log = logging.getLogger('MPTester')
#     def __init__(self, board, quads = parameters.mp.quads ):
#         super(MPTester,self).__init__(board,quads)

# class DemuxTester(MP7Controller):
#     _log = logging.getLogger('DemuxTester')
#     def __init__(self, board, quads = parameters.demux.quads ):
#         super(DemuxTester,self).__init__(board,quads)

#         self.buf     = mp7.MGTBufferNode  ( board.getNode('buffers') )
#         self.demux   = mp7.DemuxNode( board.getNode('demux') )

#     def clearBuffers( self, data_rx = None, data_tx = None ):
#         # Local references
#         ctrl = self.ctrl
#         buf = self.buf

#         time.sleep(0.1)

#         # Dump some registers, for fun
#         if False:
#             print '-'*80
#             buf_regs = buf.snapshot()
#             for n in sorted(buf_regs.iterkeys()):
#                 print n,':',hex(buf_regs[n])
#             print '-'*80

#         self.log.info( 'Clearing quad: %s', self.quads)

#         for link in self.links:
#             ctrl.selectLink( link )

#             buf.clearRx()
#             buf.clearTx()

#             if data_rx:
#                 buf.uploadValidRx( data_rx[link] )
#             if data_tx:
#                 buf.uploadValidTx( data_tx[link] )

#     #---
#     def readBuffers(self, path="", depth = parameters.defaults.mgtBuffersDepth ):
#         quads = self.quads

#         # Local references
#         ctrl = self.ctrl
#         buf = self.buf

#         # wpb = 6

#         # pull the captured data out of the buffers
#         values = {}
#         rxData = []
#         txData = []

#         rxData = dataio.BoardData( self.board.id() )
#         txData = dataio.BoardData( self.board.id() )

#         self.log.info('Reading quads: %s (%d words each)',quads,depth)

#         for link in self.links:
#             ctrl.selectLink( link )

#             vRx = buf.downloadValidRx( depth )
#             rxData.addlink( link, vRx )

#             vTx = buf.downloadValidTx( depth )
#             txData.addlink( link, vTx )

#         if path: os.system('mkdir -p '+path)

#         dataio.writeValid(rxData, os.path.join(path,'rx_summary.txt'))
#         dataio.writeValid(txData, os.path.join(path,'tx_summary.txt'))

#     #---
#     def captureAndDump(self, path, capture=True, depth=parameters.defaults.mgtBuffersDepth, data_rx=None, data_tx=None ):
#         '''
#         if cap_depth is 0x0 or none, don't capture
#         '''
#         if data_rx and isinstance(data_rx.values()[0], dict):
#             raise TypeError("data_rx is a dict of dict!!!")
#         if data_tx and isinstance(data_tx.values()[0], dict):
#             raise TypeError("data_tx is a dict of dict!!!")

#         # Local references
#         ctrl = self.ctrl
#         ttc = self.ttc
#         buf = self.buf
#         demux = self.demux

#         self.clearBuffers( data_rx, data_tx  )
#         # clear the demux counters
#         demux.getNode("ctrl.ctrl.clr_err").write(0x1)
#         demux.getNode("ctrl.ctrl.resync").write(0x1)
#         demux.getNode("ctrl.ctrl.clr_err").write(0x0)
#         demux.getNode("ctrl.ctrl.resync").write(0x0)
#         demux.getClient().dispatch()

#         if capture:
#             self.log.info('Capturing buffers')
#             # Capture the buffers
#             ttc.capture(.1)
#             # And check it's done
#             buf.waitCaptureDone()
#             self.log.info('Capture completed')

#         clk40_locked = ctrl.clock40Locked()
#         self.log.info('Clock 40 is %s',('unlocked','locked')[clk40_locked])

#         #bc0_err = ttc.getNode('ttc.stat0.bc0_err').read()
#         #ttc.getClient().dispatch()
#         #print "BC0 error is",bc0_err.value()

#         # Dump
#         if path:
#             self.snapshotBuffers(path, depth=depth)
