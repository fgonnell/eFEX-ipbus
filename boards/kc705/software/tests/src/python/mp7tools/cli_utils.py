import argparse
import buffers

def convert( string ):
    if string.startswith("0x") or string.startswith("0X"):
        return int(string,16)
    elif string.startswith("0"):
        return int(string,8)
    else:
        return int(string)

# custom class to validate 'exec' variables
class ExecVarAction(argparse.Action):

    def __call__(self, parser, namespace, values, option_string=None):
        errors=[ "'%s'"%v for v in values if '=' not in v]
        if len(errors): #raise ValueError(self.dest,'stoca')
            parser.error('The following arguments are not in <key>=<value> format: %s' % (','.join(errors)) )
        setattr(namespace, self.dest, values)

class BxRangeTupleAction(argparse.Action):

    def __call__(self, parser, namespace, values, option_string=None):

        tokens = values.split(',')
        if len(tokens) == 1:
            try:
                start_bx = convert(tokens[0])
                end_bx = None
            except StandardError as e:
                parser.error(str(e))
        elif len(tokens) == 2:
            try:
                start_bx,end_bx=( convert(t) for t in tokens )
            except StandardError as e:
                parser.error(str(e))

            if start_bx >= end_bx:
                parser.error('Range must fulfill: start < end')
        else:
            raise ValueError('Wrong format. Should be: %s=first,last' % self.dest)

        setattr(namespace, self.dest, (start_bx,end_bx) )

class IntListAction(argparse.Action):
    def __init__(self, *args, **kwargs):
        super(IntListAction, self).__init__(*args, **kwargs)
        # self._var  = var
        # self._sep  = sep
        # self._dash = dash
        self._sep  = ','
        self._dash = '-'

    def __call__(self, parser, namespace, values, option_string=None):

        numbers=[]
        items = values.split(self._sep)
        for item in items:
            nums = item.split(self._dash)
            if len(nums) == 1:
                # single number
                numbers.append(int(item))
            elif len(nums) == 2:
                i = int(nums[0])
                j = int(nums[1])
                if i > j:
                    parser.error('Invalid interval '+item)
                numbers.extend(range(i,j+1))
            else:
               parser.error('Malformed option (comma separated list expected): %s' % values)

        setattr(namespace, self.dest, numbers)

class BufferURIAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):

        try:
            uri = buffers.BoardDataSource.validateDataUri(values)
        except RuntimeError as re:
            parser.error(str(re))

        setattr(namespace, self.dest, uri)          