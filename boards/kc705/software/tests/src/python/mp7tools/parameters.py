class defaults:
    nquads = 18
    nlinks = nquads*4
    # All 18 quads
    quads = range(nquads)
    # All 4 channels
    chans = range(4)
    # mgt buffer depth
    mgtBuffersDepth = 0x400

    # clockratio
    clockRatio = 6

class mp(defaults):
    pass

class pp(defaults):
    # instantiated quads
    nquads = 9
    nlinks = nquads*4
    # All 18 quads
    quads = range(nquads)
    # All 4 channels
    chans = range(4)
    #
    ppRamDepth = 0xa00

class demux(defaults):
    # instantiated quads
    nquads = 9
    nlinks = nquads*4
    # All 18 quads
    quads = range(nquads)
    # All 4 channels
    chans = range(4)
