#include "uhal/uhal.hpp"
#include "mp7/MP7Controller.hpp"
#include <iostream>

int main()
{
//  uhal::ConnectionManager cm ( "file://etc/uhal/connections904.xml" );
//  uhal::HwInterface hw = cm.getDevice ( "MP7_690_TX_PP_TMUX" );
//  uhal::ValWord<uint32_t> id = hw.getNode ( "ctrl.id" ).read();
//  hw.dispatch();
//  std::cout << "0x" << std::hex << id << std::endl;
//  //---
//  return 0;

    uhal::HwInterface b = uhal::ConnectionManager::getDevice("MP7_SIM_JULY","ipbusudp-2.0://192.168.201.2:50001","file://${MP7_TESTS}/etc/mp7/uhal/mp7_july/mp7_infra.xml");
    mp7::MP7Controller c(b);
    c.reset();
}

