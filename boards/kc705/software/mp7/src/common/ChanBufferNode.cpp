/* 
 * File:   BufferNode.cpp
 * Author: ale
 * 
 * Created on May 5, 2014, 11:04 AM
 */

#include "mp7/ChanBufferNode.hpp"

// uHal Headers
#include "uhal/log/log.hpp"

// C++ Headers
#include <algorithm>

// MP7 Headers
#include "mp7/exception.hpp"
#include "mp7/Utilities.hpp"

// Namespace resolution
using namespace std;
using namespace uhal;

namespace mp7 {
UHAL_REGISTER_DERIVED_NODE( ChanBufferNode );

ChanBufferNode::ChanBufferNode(const uhal::Node& aNode) :
    uhal::Node(aNode), mSize(aNode.getNode("buffer.data").getSize() / 2) {

}

ChanBufferNode::~ChanBufferNode() {
//  std::cout << this << " ~BufferNode" << std::endl;
}

uint32_t
ChanBufferNode::getSize() const {
  return mSize;
}

uint32_t
ChanBufferNode::getMaxSize() const {
  return this->getNode("buffer.data").getSize() / 2;
}

void
ChanBufferNode::setSize(size_t size) {
  if ( size == 0 ) {
    mSize = this->getMaxSize();
    return;
  } else if ( size > this->getMaxSize() ) {
    exception::BufferSizeExceeded lExc;
    log(lExc,"The size of the buffers cannot be larger then ", uhal::Integer(this->getMaxSize()));
  } else {
    mSize = size;
  }
}

ChanBufferNode::BufMode
ChanBufferNode::getBufferMode() const {
  uhal::ValWord< uint32_t > mode = this->getNode("csr.mode.mode").read();
  this->getClient().dispatch();
  
  return (ChanBufferNode::BufMode)mode.value();
}

ChanBufferNode::DataSrc
ChanBufferNode::getDataSrc() const {
  uhal::ValWord< uint32_t > src =  this->getNode("csr.mode.datasrc").read();
  this->getClient().dispatch();
  
  return (ChanBufferNode::DataSrc)src.value();
}

void
ChanBufferNode::waitCaptureDone() const {
  uhal::ValWord< uint32_t > cap_done(0);
  int countdown = 100;

  while (countdown > 0) {
    cap_done = this->getNode("csr.stat.cap_done").read();
    this->getClient().dispatch();

    if (cap_done.value()) break;

    millisleep(1);
    countdown--;

  }

  if (countdown == 0) {
    throw runtime_error("timed out waiting for buffer capture done signal");
  }

  log(Notice(), "Capture completed");
}

void
ChanBufferNode::configure(BufMode aMode, DataSrc aDataSrc) const {

  // Set the buffer mode
  this->getNode("csr.mode.mode").write(aMode);
  // Set the datasrc
  this->getNode("csr.mode.datasrc").write(aDataSrc);

  // Memento for the future
  // getNode("csr.mode.daq_bank").write(0x1);
  this->getClient().dispatch();
}

void
ChanBufferNode::setRange(uint32_t trig_bx, uint32_t max_word) const {
  // add a max length check here
  if (max_word >= mSize) {
    exception::BufferConfigError lExc;
    log(lExc, "Number of frames to capture larger then buffer size");
    throw lExc;
  }
  
  max_word = std::min(max_word, (uint32_t) (mSize - 1));
  this->getNode("csr.mode.trig_bx").write(trig_bx);
  this->getNode("csr.mode.max_word").write(max_word);
  this->getClient().dispatch();
}

void
ChanBufferNode::writeRaw(std::vector<uint32_t> aRawData) const {
  // set the write pointer to 0
  this->getNode("buffer.addr").write(0x0);
  // Upload data into the buffer
  this->getNode("buffer.data").writeBlock(aRawData);
  this->getClient().dispatch();
}

std::vector<uint32_t>
ChanBufferNode::readRaw(uint32_t aSize) const {
  // Set the port pointer to 0
  this->getNode("buffer.addr").write(0x0);
  // Block read from the data register
  uhal::ValVector< uint32_t > valid_data = this->getNode("buffer.data").readBlock(aSize);
  this->getClient().dispatch();
  return valid_data.value();
}

void
ChanBufferNode::clear() const {
  vector<uint32_t> zeroes(mSize * 2, 0x0);
  writeRaw(zeroes);
}

void
ChanBufferNode::upload(std::vector<uint32_t> aData) const {

  if (aData.size() > mSize) {
    exception::BufferSizeExceeded lExc;
    log(lExc, "Data to upload bigger than buffersize");
    throw lExc;
  }
//  size_t size = std::min(mSize, aData.size());
  size_t size = aData.size();
  // Prepare the data to write to the port ram
  vector<uint32_t> valid_data(size * 2);
  
  for (size_t i(0); i < size; ++i) {
    // Even address: 16 LSB bits
    valid_data[2 * i] = aData[i] & 0xffff;
    // Odd address: 1 valid + 16 MSB
    valid_data[2 * i + 1] = (aData[i] >> 16) | (0x1 << 16);
  }

  writeRaw(valid_data);
}

std::vector<uint32_t>
ChanBufferNode::download(size_t aSize) const {
  if (aSize > mSize) {
    exception::BufferSizeExceeded lExc;
    log(lExc, "Data to download bigger than buffersize");
    throw lExc;
  }

  // Don't read more than the real depth
  vector<uint32_t> valid_data = readRaw(aSize * 2);
  vector<uint32_t> data(aSize);

  for (size_t i(0); i < data.size(); ++i) {
    data[i] = valid_data[2 * i];
    // drop the valid bit and shift by 16
    data[i] += (valid_data[2 * i + 1] & 0xffff) << 16;
  }

  return data;
}

void
ChanBufferNode::uploadValid(std::vector<uint64_t> aData) const {
  if (aData.size() > mSize) {
    exception::BufferSizeExceeded lExc;
    log(lExc, "Data to upload bigger than buffersize");
    throw lExc;
  }
//  size_t size = std::min(mSize, aData.size());
  size_t size = aData.size();
  // Prepare the data to write to the port ram
  vector<uint32_t> valid_data(size * 2);
  
  for (size_t i(0); i < size; ++i) {
    // Even address: 16 LSB bits
    valid_data[2 * i] = aData[i] & 0xffff;
    // Odd address: 1 valid + 16 MSB.
    valid_data[2 * i + 1] = (aData[i] >> 16) & 0x1ffff;
  }

  writeRaw(valid_data);
}

std::vector<uint64_t>
ChanBufferNode::downloadValid(size_t aSize) const {
  if (aSize > mSize) {
    exception::BufferSizeExceeded lExc;
    log(lExc, "Data to download bigger than buffersize");
    throw lExc;
  }

  // Don't read more than the real depth
//  uint32_t size = std::min(aSize, mSize);
//  vector<uint32_t> valid_data = readRaw(size * 2);
//  vector<uint64_t> data(size);
  
  vector<uint32_t> valid_data = readRaw(aSize * 2);
  vector<uint64_t> data(aSize);

  for (size_t i(0); i < data.size(); ++i) {
    data[i] = valid_data[2 * i];
    // cast to 64bits and shift by 16
    data[i] += ((uint64_t) valid_data[2 * i + 1]) << 16;
  }

  return data;
}

}

