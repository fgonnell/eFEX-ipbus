#include "mp7/ClockingXENode.hpp"

// C++ Headers
#include <stdexcept>

// MP7 Headers
#include "mp7/Utilities.hpp"
#include "mp7/exception.hpp"

// uHal headers
#include "uhal/ValMem.hpp"
#include "mp7/SI570Node.hpp"
#include "mp7/SI5326Node.hpp"

// Namespace resolution
using namespace std;
using namespace uhal;

namespace mp7 {

// DerivedNode registration
UHAL_REGISTER_DERIVED_NODE(ClockingXENode);

ClockingXENode::ConfigMap ClockingXENode::makeDefaultConfigs() {
    ConfigMap configs;
    
    Config internal = {
        Disconnected,
//        Oscillator,
        "",
        "",
        ""
            
    };
    configs["internal"] = internal;
        
//    Config external = {
//        ExternalAMC13,
////        Oscillator,
//        "${MP7_TESTS}/etc/mp7/sicfg/si5326/MP7_SI5326_20130606_40.1MHz_CKIN1_to_160.4MHz_CKOUT1_NoIncDec_Regs.txt"
//    };
//    configs["external"] = external;

    return configs;
}

const boost::unordered_map<std::string, ClockingXENode::Config> ClockingXENode::mConfigurations = makeDefaultConfigs();

ClockingXENode::ClockingXENode(const uhal::Node& node) :
uhal::Node(node) {
}

ClockingXENode::~ClockingXENode() {
}

void
ClockingXENode::configure(const std::string aConfig) const {
    
    ConfigMap::const_iterator it = mConfigurations.find(aConfig);
    if (it == mConfigurations.end()) {
        exception::MP7HelperException lExc;
        log(lExc, "Configuration ", aConfig, " could not be found");
        throw lExc;
    }
    
//    const SI5326Node& si5326Top = getNode<SI5326Node>("i2c_si5326_top");
//    const SI5326Node& si5326Bot = getNode<SI5326Node>("i2c_si5326_bot");
//    const SI570Node&  si570Bot  = getNode<SI570Node> ("i2c_si570_bot");
    
}

void
ClockingXENode::configureXpoint(Clk40Select aClk40Src) const {

    switch (aClk40Src) {
        case ExternalAMC13:
            // Clock 40 routing from AMC13 to FPGA and both SI5326
            this->configureU36(3, 3, 3, 3);
            break;
        case ExternalMCH:
            // Clock 40 routing from MCH, external clock generator
            this->configureU36(1, 1, 0, 0);
            break;
        case Disconnected:
            // From internal oscillator
            // U36 don't matter. Best to have a default anyway?
            // 0,0,0,0 is the poweron value
            this->configureU36(0, 0, 0, 0);
            break;
        default:
            throw runtime_error("Invalid clock 40 source");
    }

}

void
ClockingXENode::configureUX(const std::string& aChip, uint SelForOut0, uint SelForOut1, uint SelForOut2, uint SelForOut3) const {

    const Node& ctrl = this->getNode("ctrl");
    ctrl.getNode("selforout1_" + aChip).write(SelForOut0);
    ctrl.getNode("selforout2_" + aChip).write(SelForOut1);
    ctrl.getNode("selforout3_" + aChip).write(SelForOut2);
    ctrl.getNode("selforout4_" + aChip).write(SelForOut3);

    ctrl.getNode("prog_" + aChip).write(1);
    this->getClient().dispatch();
    usleep(10);
    ctrl.getNode("prog_" + aChip).write(0);
    this->getClient().dispatch();
    // wait for the cross point switch to assert its done signal
    const string stat_done = "stat.done_" + aChip;
    ValWord< uint32_t > done(0);
    int countdown = 100;

    while (countdown > 0) {
        done = this->getNode(stat_done).read();
        this->getClient().dispatch();

        if (done) {
            break;
        }

        --countdown;
    }

    if (countdown == 0) {
        exception::XpointConfigTimeout lExc;
        log(lExc, "Timed out while waiting for Xpoint ", aChip, " to complete configuration ( 100 tries)");
        throw lExc;
    }
}

void
ClockingXENode::configureU36(uint SelForOut0, uint SelForOut1, uint SelForOut2, uint SelForOut3) const {
    // Inputs to xpoint_u36           	 Outputs to xpoint_u36
    // Input 0 = clock from FPGA   	         Output 0 = TCLKB 
    // Input 1 = TCLK-C               	 Output 1 = clk3 (clk40 to fpga)
    // Input 2 = TCLK-A               	 Output 2 = clk2 (top)
    // Input 3 = FCLK-A               	 Output 3 = clk1 (bot)
    // ---------------------------------------------------------------------
    // Wish to send MCH TCLK-A (input-2) to si5326 (output-0) and for all
    // other outputs to be driven by si5326 (input-0)
    // ---------------------------------------------------------------------
    this->configureUX("u36", SelForOut0, SelForOut1, SelForOut2, SelForOut3);
}

void
ClockingXENode::si5326TopReset() const {
    this->si5326ChipReset("top");
}

void
ClockingXENode::si5326TopWaitConfigured(bool aMustLock, uint32_t aMaxTries) const {
    this->si5326ChipWaitConfigured("top", aMustLock, aMaxTries);
}

bool
ClockingXENode::si5326TopLossOfLock() const {
    return this->si5326ChipLossOfLock("top");
}

bool
ClockingXENode::si5326TopInterrupt() const {
    return this->si5326ChipInterrupt("top");
}

void
ClockingXENode::si5326BottomReset() const {
    this->si5326ChipReset("bot");
}

void
ClockingXENode::si5326BottomWaitConfigured(bool aMustLock, uint32_t aMaxTries) const {
    this->si5326ChipWaitConfigured("bot", aMustLock, aMaxTries);
}

bool
ClockingXENode::si5326BottomLossOfLock() const {
    return this->si5326ChipLossOfLock("bot");
}

bool
ClockingXENode::si5326BottomInterrupt() const {
    return this->si5326ChipInterrupt("bot");
}

void
ClockingXENode::si5326ChipReset(const std::string& aChip) const {
    const uhal::Node& nReset = this->getNode("ctrl.rst_" + aChip + "_si5326");

    // Reset the si5326
    nReset.write(0);
    nReset.getClient().dispatch();
    // minimum reset pulse width is 1 microsecond, we go for 5
    usleep(5);
    nReset.write(1);
    nReset.getClient().dispatch();
}

void
ClockingXENode::si5326ChipWaitConfigured(const std::string& aChip, bool aMustLock, uint32_t aMaxTries) const {
    const uhal::Node& nLol = this->getNode("stat.si5326_" + aChip + "_lol");
    const uhal::Node& nInt = this->getNode("stat.si5326_" + aChip + "_int");

    uint32_t countdown(aMaxTries);
    while (countdown > 0) {
        ValWord<uint32_t> si5326_lol = nLol.read();
        ValWord<uint32_t> si5326_int = nInt.read();
        this->getClient().dispatch();

        if ((si5326_lol.value() != (uint32_t) aMustLock) && si5326_int.value() == 0) {
            break;
        }

        millisleep(1);
        --countdown;
    }

    if (countdown == 0) {
        log(Error(), "timed out waiting for si5326 to recover from configuration");
        exception::SI5326ConfigurationTimeout lExc;
        log(lExc, "Timed out while waiting for SI5326 to complete configuration (", uhal::Integer(aMaxTries), " ms)");
        throw lExc;
    } else {
        log(Notice(), "SI5326 finished configuring after ", uhal::Integer(aMaxTries - countdown), " ms");
    }
}

bool
ClockingXENode::si5326ChipLossOfLock(const std::string& aChip) const {
    ValWord<uint32_t> si5326_lol = this->getNode("stat.si5326_" + aChip + "_lol").read();
    this->getClient().dispatch();
    return ( bool) si5326_lol;
}

bool
ClockingXENode::si5326ChipInterrupt(const std::string& aChip) const {
    ValWord<uint32_t> si5326_int = this->getNode("stat.si5326_" + aChip + "_int").read();
    this->getClient().dispatch();
    return ( bool) si5326_int;
}

}

