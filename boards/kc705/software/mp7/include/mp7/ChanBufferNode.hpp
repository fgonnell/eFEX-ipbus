/* 
 * File:   BufferManager.hpp
 * Author: ale
 *
 * Created on May 5, 2014, 11:04 AM
 */

#ifndef _mp7_ChanBufferNode_hpp_
#define	_mp7_ChanBufferNode_hpp_

// uHAL Headers
#include "uhal/DerivedNode.hpp"

namespace mp7 {

class ChanBufferNode : public uhal::Node {
    UHAL_DERIVEDNODE( ChanBufferNode );
public:
  /* @class BufferManager
   * @brief Handles path & buffer configurations
   */
  
  // PUBLIC ENUMS

  enum BufMode {
    Latency = 0, // Latency buffer
    Capture = 1, // Capture buffer
    PlayOnce = 2, // Playback
    PlayLoop = 3 // Repeating playback
  };

  enum DataSrc {
    Input = 0, // input data
    Buffer = 1, // buffer playback
    Pattern = 2, // Hard-coded pattern
    Zeroes = 3 // 
  };
  
  ChanBufferNode(const uhal::Node& aNode);
  virtual ~ChanBufferNode();

  /// returns the current buffer size
  uint32_t getSize() const;
  
  /// return the maximum buffer size
  uint32_t getMaxSize() const;

  /// sets the size of the buffers to be used. Must be less than max size
  void setSize( size_t size = 0x0 );
  
  /// Get current buffer mode
  BufMode getBufferMode() const;
  
  /// Get datasrc for the current buffer
  DataSrc getDataSrc() const;
  
  
  /// wait for the buffer capture to be completed
  void waitCaptureDone() const;

  /// Configure buffer mode and data source
  void configure(BufMode aMode, DataSrc aDataSrc) const;

  // Set capture/playback range
  void setRange(uint32_t trig_bx, uint32_t words) const;

  /// Clear the buffer block. Fills it with zeroes
  void clear() const;

  /// Uploads data into the buffer up to the buffer maximum size
  void upload(std::vector<uint32_t> aData) const;
  std::vector<uint32_t> download(size_t aSize) const;

  void uploadValid(std::vector<uint64_t> aData) const;
  std::vector<uint64_t> downloadValid(size_t aSize) const;

  /// Raw buffer access
  std::vector<uint32_t> readRaw(uint32_t aSize) const;
  void writeRaw(std::vector<uint32_t> aRawData) const;

private:

//  const uhal::Node* mCSR;
//  const uhal::Node* mBuffer;

  size_t mSize;

};

}

#endif	/* _mp7_ChanBufferNode_hpp_ */

