/* 
 * File:   MP7Controller.hpp
 * Author: ale
 *
 * Created on July 17, 2014, 9:23 PM
 */

#ifndef __mp7_mp7controller_hpp__
#define	__mp7_mp7controller_hpp__

// uHAL Headers
#include "uhal/HwInterface.hpp"

// Boost Headers
#include "boost/lexical_cast.hpp"

namespace mp7 {


// Forward declarations
class CtrlNode;

class ClockingNode;
class TTCNode;

class DatapathNode;
class AlignmentNode;
class GTHQuadNode;

class ChanBufferNode;
class MmcPipeInterface;

class MP7Controller : public uhal::HwInterface {
public:

    class Parameters : public boost::unordered_map<std::string, std::string> {
    public:
        Parameters();
        ~Parameters();
        
        template<typename T>
        void set( const std::string& aKey, const T& aValue ) {
            this->operator[](aKey) = boost::lexical_cast<std::string>(aValue);
        }
        
        template<typename T>
        T get(const std::string& aKey) const {
            const_iterator it = this->find(aKey);
            //TODO runtime_error -> dedicated error
            if (it == this->end()) throw std::runtime_error("Parameter not found");

            return boost::lexical_cast<T>(it->second);
        }

    };
    MP7Controller(uhal::HwInterface aInterface);

    virtual ~MP7Controller();

    /**
     * 
     */
    void reset(/* clock configuration options */);
    /**
     * Human-readable report of MGT clock readings
     * @return map of 
     */
    boost::unordered_map<std::string, std::string> refClkReport();

protected:
    //! Configures TTC block and waits for BC0 lock 
    void configureTTC(bool aEnable, bool aBC0Internal);

    //!
    void configureClocking( bool aExternalClk40 = false, const std::string& aClkCfg="internal");

private:

    void Constructor();

    //! 
    boost::unordered_map<std::string, uint32_t> mGenerics;

    //! List of regions instantiated in firmware
    std::vector<uint32_t> mRegions;

    //! Regions with buffers
    std::vector<uint32_t> mBufferRegions;

    //! Regions with buggers and MGTs
    std::vector<uint32_t> mMGTRegions;

    //! List of channels
    std::vector<uint32_t> mChannels;

    //! List of channels with buffers
    std::vector<uint32_t> mBufferChannels;

    //! List of channels with mgt block
    std::vector<uint32_t> mMGTChannels;
    
    //! MGT Input Mask
    std::vector<uint32_t> mActiveChannels;

    //! Specialised Nodes
    const CtrlNode * mCtrl;
    const ClockingNode * mClocking;
    const TTCNode * mTTC;

    const DatapathNode * mDatapath;
    const AlignmentNode * mAlign;
    const GTHQuadNode * mMGT;

    const ChanBufferNode * mChanBuffer;
    const MmcPipeInterface * mMmcPipe;

    //! Mutex for multithread-safe environment
    boost::mutex mMutex;
};

}

#endif	/* __mp7_mp7controller_hpp__ */

