/* 
 * File:   Parameters.hpp
 * Author: ale
 *
 * Created on July 20, 2014, 9:50 AM
 */

#ifndef __mp7_parameters_hpp__
#define	__mp7_parameters_hpp__

// C++ Headers
#include <set>

// Boost Headers
#include <boost/unordered_map.hpp>
#include <boost/any.hpp>

namespace mp7 {

class Parameters : private boost::unordered_map<std::string, boost::any> {
    typedef  boost::unordered_map<std::string, boost::any> parent;
public:
    typedef boost::any Object;

    Parameters() {
    }

    virtual ~Parameters() {
    }

    template<typename T>
    void set(const std::string& aKey, const T& aValue) {
        this->operator[](aKey) = aValue;
    }

    template<typename T>
    T get(const std::string& aKey) const {
        const_iterator it = this->find(aKey);
        //TODO runtime_error -> dedicated error
        if (it == this->end()) throw std::runtime_error("Parameter not found");

        return boost::any_cast<T>(it->second);
    }
    
    std::set<std::string> names() const {
        std::set<std::string> names;
        for( const_iterator it = begin(); it != end(); ++it)
            names.insert(it->first);
        
        return names;
    }

    void set(const std::string& aKey, const char* aValue ) {
        this->parent::operator[](aKey) = std::string(aValue) ;
    }
    
    Object& operator []( const std::string& aKey ){
        return this->parent::operator[](aKey);
    }
};


}

#endif	/* __mp7_parameters_hpp__ */

