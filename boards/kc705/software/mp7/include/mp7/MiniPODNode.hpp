#ifndef _mp7_MiniPODNode_hpp_
#define _mp7_MiniPODNode_hpp_

#include "mp7/OpenCoresI2C.hpp"
#include "mp7/Measurement.hpp"

#include <vector>
#include <map>
#include <string>
#include <time.h>
#include <ostream>


namespace mp7
{
  struct MiniPODinfo
  {
    uint8_t type_identifier;
    uint8_t module_description;
    uint8_t required_power_supplies;
    Measurement maximum_operating_temperature;
    Measurement minimum_bit_rate;
    Measurement maximum_bit_rate;
    Measurement wavelength;
    uint8_t supported_flags;
    uint16_t supported_monitors;
    uint16_t supported_controls;
    std::string vendor_name;
    std::vector<uint8_t> vendor_ieee_id;
    std::string vendor_part_number;
    std::string vendor_revision_number;
    std::string vendor_serial_number;
    tm vendor_date_code;
    uint16_t vendor_firmware_revision;
  };

  class MiniPODNode : public OpenCoresI2C
  {
      UHAL_DERIVEDNODE ( MiniPODNode );

    public:
      MiniPODNode ( const uhal::Node& aNode );
      virtual ~MiniPODNode();

      Measurement get3v3();
      Measurement get2v5();
      Measurement getTemp();
      Measurement getOnTime();
      std::vector < Measurement > getOpticalPowers();

      void setChannelPolarity ( const uint32_t& aMask );
      void disableChannel ( const uint32_t& aMask );
      void disableSquelch ( const bool& aDisabled );

      std::pair< bool , bool > getAlarmTemp();
      std::pair< bool , bool > getAlarm3v3();
      std::pair< bool , bool > getAlarm2v5();
      std::vector< bool > getAlarmLOS();
      std::vector< std::pair< bool , bool > > getAlarmOpticalPower();

      MiniPODinfo getInfo();

    protected:
      uint16_t getUint16 ( const uint32_t& aMSB , const uint32_t& aLSB );
      std::vector<uint8_t> block_read ( const uint32_t& aI2CbusAddress , const uint32_t aSize );
  };



  class MiniPODTxNode : public MiniPODNode
  {
      UHAL_DERIVEDNODE ( MiniPODTxNode );
    public:
      MiniPODTxNode ( const uhal::Node& aNode );
      virtual ~MiniPODTxNode();

      std::vector < Measurement > getBiasCurrents();

      void setInputEqualization ( const double& aPercentage );

      void marginMode ( const bool& aEnabled );

      std::vector< bool > getAlarmFault();
      std::vector< std::pair< bool , bool > > getAlarmBiasCurrent();


  };



  class MiniPODRxNode : public MiniPODNode
  {
      UHAL_DERIVEDNODE ( MiniPODRxNode );

    public:
      MiniPODRxNode ( const uhal::Node& aNode );
      virtual ~MiniPODRxNode();

      void setDeemphasis ( const double& aPercentage );
      void setOutputAmplitude ( const double& aPercentage );

  };
}

std::ostream& operator<< ( std::ostream& aStream , const mp7::MiniPODinfo& aInfo );

#endif

