

#ifndef _MP7_Measurement_hpp_
#define _MP7_Measurement_hpp_

#include <string>
#include <ostream>
#include <cmath>
#include <limits>

//#define NaN (0.0 / 0)
#define NaN std::numeric_limits<double>::quiet_NaN()

namespace mp7 {

class Measurement {
public:
    //    Measurement() : value ( NaN ) , units ( "" ) , tolerence ( NaN ) , tolerence_units ( "" ) {}
    //    Measurement ( const double& aValue , const std::string& aUnits ) : value ( aValue ) , units ( aUnits ) , tolerence ( NaN ) , tolerence_units ( "" ) {}
    //    Measurement ( const double& aValue , const std::string& aUnits  , const double& aTolerence ) : value ( aValue ) , units ( aUnits ) , tolerence ( aTolerence ) , tolerence_units ( aUnits ) {}

    Measurement(const double& aValue = NaN, const std::string& aUnits = "", const double& aTolerence = NaN, const std::string& aTolerenceUnits = "") : value(aValue), units(aUnits), tolerence(aTolerence), tolerence_units(aTolerenceUnits) {
    }

    double value;
    std::string units;

    double tolerence;
    std::string tolerence_units;

};

std::ostream& operator<<(std::ostream& aStream, const Measurement& aMeasurement);

//std::ostream& operator<<(std::ostream& aStream, const Measurement& aMeasurement) {
//    if (isnan(aMeasurement.value)) {
//        aStream << "{undefined}";
//    } else {
//        aStream << std::dec << aMeasurement.value << aMeasurement.units;
//
//        if (not isnan(aMeasurement.tolerence)) {
//            aStream << " (+/-" << aMeasurement.tolerence << aMeasurement.tolerence_units << ")";
//        }
//    }
//
//    return aStream;
//}

}

#endif /* _MP7_Measurement_hpp_ */

