/* 
 * File:   LinkDRPManager.hpp
 * Author: ale
 *
 * Created on June 3, 2014, 2:03 PM
 */

#ifndef _mp7_LinkDRPManager_hpp_
#define	_mp7_LinkDRPManager_hpp_

// uHAL Headers
#include "uhal/Node.hpp"

class LinkDRPManager {
public:
  LinkDRPManager( const uhal::Node& aDRPChanNode, const uhal::Node& aDRPCommonNode);
  virtual ~LinkDRPManager();
private:
  
  const uhal::Node& mChannel;
  const uhal::Node& mCommon;

};

#endif	/* _mp7_LinkDRPManager_hpp_ */

