#ifndef _mp7_DatapathNode_hpp_
#define	_mp7_DatapathNode_hpp_

// uHAL Headers
#include "uhal/DerivedNode.hpp"

// MP7 Headers
#include "mp7/PathManager.hpp"
#include "mp7/LinkDRPManager.hpp"
#include "LinkDRPManager.hpp"

namespace mp7 {

/*!
 * @class DatapathNode
 * @brief Class to control the transciever buffer interface
 *
 * buffer mode: 0: latency buf; 1: capture; 2: play once; 3: play repeat
 * data source: 0: input data; 1: buffer playback; 2: pattern gen; 3: zeroes
 *
 * Data source: 
 * @author Alessandro Thea
 * @date 2013
 */


class DatapathNode : public uhal::Node {
    UHAL_DERIVEDNODE(DatapathNode);
public:

    // PUBLIC METHODS
    DatapathNode(const uhal::Node& aNode);

    /// Copy constructor required to handle *mManager copies
    DatapathNode(const DatapathNode& aNode);
    virtual ~DatapathNode();
    
    const LinkDRPManager& drpManager() const;

private:

    LinkDRPManager* mDRPManager;

};

}

#endif	/* _mp7_DatapathNode_hpp_ */


