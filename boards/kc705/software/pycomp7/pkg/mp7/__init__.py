import uhal
from _core import *

##################################################
# Pythonic additions to uhal::exception API

def _exception_to_string(self):
  # what returns the exception string with one \n too many
  return self.what[:-1]

exception.__str__ = _exception_to_string

##########################################################
# Utility function to take a snapshot of all the registers
def snapshot(self, aRegex = '' ):
  if aRegex :
    nodes = self.getNodes(aRegex)
  else:
    nodes = self.getNodes()

  vwords = [ (node,self.getNode(node).read()) for node in nodes ]
  self.getClient().dispatch()
  return dict( [ (n,v.value()) for n,v in vwords ] )

_classes = [
  ChanBufferNode,
  ClockingNode,
  ClockingXENode,
  CtrlNode,
  TTCNode,
  OpenCoresI2C,
  SI5326Node,
  SI570Node,

  GTHQuadNode,
  GTXQuadNode,
  DatapathNode,
  AlignmentNode,

  DemuxNode,
  PPRamNode,

  MiniPODNode,
  MmcPipeInterface,
  Firmware,
  XilinxBitStream,
  XilinxBitFile,
  XilinxBinFile,
  ]

for cl in _classes:
  setattr(cl,'snapshot',snapshot)

for name,enum in ChanBufferNode.DataSrc.names.iteritems():
  setattr(ChanBufferNode,name,enum)

for name,enum in ChanBufferNode.BufMode.names.iteritems():
  setattr(ChanBufferNode,name,enum)

for name,enum in TTCNode.FreqClockChannel.names.iteritems():
  setattr(TTCNode,name,enum)

