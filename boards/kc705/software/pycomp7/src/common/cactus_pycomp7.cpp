// Boost Headers
#include "boost/lexical_cast.hpp"
#include "boost/algorithm/string.hpp"
#include "boost/python.hpp"
#include "boost/python/module.hpp"
#include "boost/python/def.hpp"
#include "boost/python/suite/indexing/vector_indexing_suite.hpp"
#include "boost/python/wrapper.hpp"

// C++ Headers
#include "map"

// uHal Headers
#include "uhal/uhal.hpp"

// MP7 Headers
#include "mp7/MP7Controller.hpp"
#include "mp7/ChanBufferNode.hpp"
#include "mp7/ClockingNode.hpp"
#include "mp7/ClockingXENode.hpp"
#include "mp7/CtrlNode.hpp"
#include "mp7/GTXQuadNode.hpp"
#include "mp7/GTHQuadNode.hpp"

#include "mp7/TTCNode.hpp"
#include "mp7/SI5326Node.hpp"
#include "mp7/SI570Node.hpp"
#include "mp7/DatapathNode.hpp"
#include "mp7/PPRamNode.hpp"
#include "mp7/AlignmentNode.hpp"
#include "mp7/DemuxNode.hpp"
#include "mp7/MiniPODNode.hpp"
#include "mp7/Measurement.hpp"
#include "mp7/MmcPipeInterface.hpp"
#include "mp7/Firmware.hpp"

#include "mp7/I2CMasterNode.hpp"
#include "mp7/MiniPODMasterNode.hpp"


// Custom Python Headers
#include "mp7/python/converters_exceptions.hpp"

using namespace boost::python;

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(mp7_TTCNode_enable_overloads, enable, 0, 1)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(mp7_TTCNode_generateInternalBC0_overloads, generateInternalBC0, 0, 1)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(mp7_TTCNode_captureData_overloads, captureData, 0, 1)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(mp7_TTCNode_captureBGOs_overloads, captureBGOs, 0, 2)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(mp7_TTCNode_measureClockFreq_overloads, measureClockFreq, 1, 2)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(mp7_TTCNode_freqClk40_overloads, freqClk40, 0, 1)


BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(mp7_CTRLNODE_hardReset_overloads, hardReset, 0, 1)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(mp7_CTRLNODE_selectClk40Source_overloads, selectClk40Source, 0, 2)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(mp7_CTRLNODE_resetClk40_overloads, resetClk40, 0, 1)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(mp7_CTRLNODE_waitClk40Lock_overloads, waitClk40Lock, 0, 1)

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(mp7_CLOCKINGNODE_si5326WaitConfigured_overloads, si5326WaitConfigured, 0, 1)

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(mp7_CLOCKINGXENODE_si5326BottomWaitConfigured_overloads, si5326BottomWaitConfigured, 0, 2)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(mp7_CLOCKINGXENODE_si5326TopWaitConfigured_overloads, si5326TopWaitConfigured, 0, 2)

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(mp7_AlignmentNode_align_overloads, align, 0, 1)

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(mp7_PathManager_setSize_overloads, setSize, 0, 1)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(mp7_ChanBufferNode_setSize_overloads, setSize, 0, 1)



// *** N.B: The argument of this BOOST_PYTHON_MODULE macro MUST be the same as the name of the library created, i.e. if creating library file my_py_binds_module.so , imported in python as:
//                import my_py_binds_module
//          then would have to put
//                BOOST_PYTHON_MODULE(my_py_binds_module)
//          Otherwise, will get the error message "ImportError: dynamic module does not define init function (initmy_py_binds_module)
BOOST_PYTHON_MODULE(_core) {
    // CONVERTERS
    pycomp7::register_converters();
    // EXCEPTIONS
    pycomp7::wrap_exceptions();

    // Wrap MP7690Driver
    class_<mp7::MP7Controller, bases<uhal::HwInterface> > ("MP7Controller", init<uhal::HwInterface&>())
            .def("reset", &mp7::MP7Controller::reset)
            ;
    // Wrap the CtrlNode
    {
        scope mp7_CtrlNode_scope = class_<mp7::CtrlNode, bases<uhal::Node> > ("CtrlNode", init<const uhal::Node&>())
                .def("hardReset", (void ( mp7::CtrlNode::*) (double)) 0, mp7_CTRLNODE_hardReset_overloads())
                .def("softReset", &mp7::CtrlNode::softReset)
                .def("waitClk40Lock", (void ( mp7::CtrlNode::*) (uint32_t)) 0, mp7_CTRLNODE_waitClk40Lock_overloads())
                .def("selectClk40Source", ( void (mp7::CtrlNode::*) (bool, double)) 0, mp7_CTRLNODE_selectClk40Source_overloads())
                .def("clock40Locked", &mp7::CtrlNode::clock40Locked)
                .def("selectChannel", &mp7::CtrlNode::selectChannel)
                .def("selectRegion", &mp7::CtrlNode::selectRegion)
                .def("selectRegChan", &mp7::CtrlNode::selectRegChan)
                .def("selectLink", &mp7::CtrlNode::selectLink)
                .def("selectLinkBuffer", &mp7::CtrlNode::selectLinkBuffer)
                .def("getRegions", &mp7::CtrlNode::getRegions)
                .def("getBufferRegions", &mp7::CtrlNode::getBufferRegions)
                .def("getMGTRegions", &mp7::CtrlNode::getMGTRegions)
                .def("getGenerics", &mp7::CtrlNode::getGenerics)
                ;
        enum_<mp7::CtrlNode::BufferSelection> ("BufferSelect")
                .value("Rx", mp7::CtrlNode::Rx)
                .value("Tx", mp7::CtrlNode::Tx)
                .export_values()
                ;
        
        // Wrap CtrlNode::Clock40Sentry
        class_<mp7::CtrlNode::Clock40Guard>("Clock40Guard", init<const mp7::CtrlNode&, optional<double> >())
        ;
    }

    // Wrap TTCNode
    {
        scope mp7_TTCNode_scope = class_<mp7::TTCNode, bases<uhal::Node> > ("TTCNode", init<const uhal::Node&>())
                .def("enable", (void ( mp7::TTCNode::*) (bool)) 0x0, mp7_TTCNode_enable_overloads())
                .def("generateInternalBC0", (void ( mp7::TTCNode::*) (bool)) 0x0, mp7_TTCNode_generateInternalBC0_overloads())
                .def("clear", &mp7::TTCNode::clear)
                .def("captureData", (void ( mp7::TTCNode::*) (double)) 0x0, mp7_TTCNode_captureData_overloads())
                .def("captureBGOs", (std::vector<uint64_t> (mp7::TTCNode::*) (bool, double)) 0, mp7_TTCNode_captureBGOs_overloads())
                .def("sendBGo", &mp7::TTCNode::sendBGo)
                .def("sendBTest", &mp7::TTCNode::sendBTest)
                .def("waitBC0Lock", &mp7::TTCNode::waitBC0Lock)
                .def("measureClockFreq", (double ( mp7::TTCNode::*) (mp7::TTCNode::FreqClockChannel, double)) 0, mp7_TTCNode_measureClockFreq_overloads())
                .def("freqClk40", (double ( mp7::TTCNode::*) (double)) 0, mp7_TTCNode_freqClk40_overloads())
                .def("report", &mp7::TTCNode::report)
                ;

        enum_<mp7::TTCNode::FreqClockChannel> ("FreqClockChannel")
                .value("Clock40", mp7::TTCNode::Clock40)
                .value("RefClock", mp7::TTCNode::RefClock)
                .value("RxClock", mp7::TTCNode::RxClock)
                .value("TxClock", mp7::TTCNode::TxClock)
                .export_values()
                ;

    }
    // Wrap ClockingNode
    {
        
        // ClockingNode scoping
        scope mp7_ClockingNode_scope = class_<mp7::ClockingNode, bases<uhal::Node> > ("ClockingNode", init<const uhal::Node&>())
                .def("configure", &mp7::ClockingNode::configure)
                .def("configureXpoint", &mp7::ClockingNode::configureXpoint)
                .def("configureU3", &mp7::ClockingNode::configureU3)
                .def("configureU15", &mp7::ClockingNode::configureU15)
                .def("configureU36", &mp7::ClockingNode::configureU36)
                .def("si5326Reset", &mp7::ClockingNode::si5326Reset)
                .def("si5326WaitConfigured", (void ( mp7::ClockingNode::*) (uint32_t)) 0, mp7_CLOCKINGNODE_si5326WaitConfigured_overloads())
                .def("si5326LossOfLock", &mp7::ClockingNode::si5326LossOfLock)
                .def("si5326Interrupt", &mp7::ClockingNode::si5326Interrupt)
                ;
        enum_<mp7::ClockingNode::Clk40Select> ("Clk40Select")
                .value("ExternalAMC13", mp7::ClockingNode::ExternalAMC13)
                .value("ExternalMCH", mp7::ClockingNode::ExternalMCH)
                .value("Disconnected", mp7::ClockingNode::Disconnected)
                .export_values()
                ;
        enum_<mp7::ClockingNode::RefClkSelect> ("RefClkSelect")
                .value("Oscillator", mp7::ClockingNode::Oscillator)
                .value("ClockCleaner", mp7::ClockingNode::ClockCleaner)
                .export_values()
                ;
    }

    // Wrap ClockingXENode
    {
        // ClockingXENode scoping
        scope mp7_ClockingXENode_scope = class_<mp7::ClockingXENode, bases<uhal::Node> > ("ClockingXENode", init<const uhal::Node&>())
                .def("configure", &mp7::ClockingXENode::configure)
                .def("configureXpoint", &mp7::ClockingXENode::configureXpoint)
                .def("configureU36", &mp7::ClockingXENode::configureU36)
                .def("si5326BottomReset", &mp7::ClockingXENode::si5326BottomReset)
                .def("si5326BottomWaitConfigured", (void ( mp7::ClockingXENode::*) (bool, uint32_t)) 0, mp7_CLOCKINGXENODE_si5326BottomWaitConfigured_overloads())
                .def("si5326BottomLossOfLock", &mp7::ClockingXENode::si5326BottomLossOfLock)
                .def("si5326BottomInterrupt", &mp7::ClockingXENode::si5326BottomInterrupt)
                .def("si5326TopReset", &mp7::ClockingXENode::si5326TopReset)
                .def("si5326TopWaitConfigured", (void ( mp7::ClockingXENode::*) (bool, uint32_t)) 0, mp7_CLOCKINGXENODE_si5326TopWaitConfigured_overloads())
                .def("si5326TopLossOfLock", &mp7::ClockingXENode::si5326TopLossOfLock)
                .def("si5326TopInterrupt", &mp7::ClockingXENode::si5326TopInterrupt)
                ;
        enum_<mp7::ClockingXENode::Clk40Select> ("Clk40Select")
                .value("ExternalAMC13", mp7::ClockingXENode::ExternalAMC13)
                .value("ExternalMCH", mp7::ClockingXENode::ExternalMCH)
                .value("Disconnected", mp7::ClockingXENode::Disconnected)
                .export_values()
                ;
    }

    // Wrap OpenCoresI2C
    class_<mp7::OpenCoresI2C, bases<uhal::Node> > ("OpenCoresI2C", init<const uhal::Node&>())
            .def(init<const uhal::Node&, uint8_t>())
            .def("getI2CSlaveAddress", &mp7::OpenCoresI2C::getI2CSlaveAddress)
            .def("getI2CClockPrescale", &mp7::OpenCoresI2C::getI2CClockPrescale)
            .def("readI2C", &mp7::OpenCoresI2C::readI2C)
            .def("writeI2C", &mp7::OpenCoresI2C::readI2C)
            ;

    // Wrap opencores::I2CMasterNode
    class_<mp7::opencores::I2CBaseNode, bases<uhal::Node> > ("I2CBaseNode", init<const uhal::Node&>())
            .def("getI2CClockPrescale", &mp7::opencores::I2CBaseNode::getI2CClockPrescale)
            .def("readI2C", &mp7::opencores::I2CBaseNode::readI2C)
            .def("writeI2C", &mp7::opencores::I2CBaseNode::writeI2C)
            .def("getSlaves", &mp7::opencores::I2CBaseNode::getSlaves)
        ;

    class_<mp7::opencores::I2CMasterNode, bases<mp7::opencores::I2CBaseNode> > ("I2CMasterNode", init<const uhal::Node&>())
        .def("getSlave", &mp7::opencores::I2CMasterNode::getSlave, return_internal_reference<>())
        ;

    // Wrap opencores::I2CSlave    
    class_<mp7::opencores::I2CSlave, boost::noncopyable>("I2CSlave", no_init)
            .def("getI2CAddress", &mp7::opencores::I2CSlave::getI2CAddress)
            .def("readI2C", &mp7::opencores::I2CSlave::readI2C)
            .def("writeI2C", &mp7::opencores::I2CSlave::readI2C)
        ;
    
    // Wrap MiniPODMasterNode
    class_<mp7::MiniPODMasterNode, bases<mp7::opencores::I2CBaseNode> > ("MiniPODMasterNode", init<const uhal::Node&>())
            .def("getRxPOD", &mp7::MiniPODMasterNode::getRxPOD, return_internal_reference<>())
            .def("getTxPOD", &mp7::MiniPODMasterNode::getTxPOD, return_internal_reference<>())
            .def("getRxPODs", &mp7::MiniPODMasterNode::getRxPODs)
            .def("getTxPODs", &mp7::MiniPODMasterNode::getTxPODs)
        ;

    // Wrap MiniPODSlave
    class_<mp7::MiniPODSlave, bases<mp7::opencores::I2CSlave>, boost::noncopyable >("MiniPODSlave", no_init)
            .def("readI2C", &mp7::MiniPODSlave::readI2C)
            .def("writeI2C", &mp7::MiniPODSlave::readI2C)
            .def("get3v3", &mp7::MiniPODSlave::get3v3)
            .def("get2v5", &mp7::MiniPODSlave::get2v5)
            .def("getTemp", &mp7::MiniPODSlave::getTemp)
            .def("getOnTime", &mp7::MiniPODSlave::getOnTime)
            .def("getOpticalPowers", &mp7::MiniPODSlave::getOpticalPowers)
            .def("setChannelPolarity", &mp7::MiniPODSlave::setChannelPolarity)
            .def("disableChannel", &mp7::MiniPODSlave::disableChannel)
            .def("disableSquelch", &mp7::MiniPODSlave::disableSquelch)
            .def("getAlarmTemp", &mp7::MiniPODSlave::getAlarmTemp)
            .def("getAlarm3v3", &mp7::MiniPODSlave::getAlarm3v3)
            .def("getAlarm2v5", &mp7::MiniPODSlave::getAlarm2v5)
            .def("getAlarmLOS", &mp7::MiniPODSlave::getAlarmLOS)
            .def("getAlarmOpticalPower", &mp7::MiniPODSlave::getAlarmOpticalPower)
            .def("getInfo", &mp7::MiniPODSlave::getInfo)
        ;

    class_<mp7::MiniPODRxSlave, bases<mp7::MiniPODSlave>, boost::noncopyable >("MiniRxPODSlave", no_init);
    class_<mp7::MiniPODTxSlave, bases<mp7::MiniPODSlave>, boost::noncopyable >("MiniTxPODSlave", no_init);

    
    // Wrap SI570Slave
    class_<mp7::SI570Slave, bases<mp7::opencores::I2CSlave>, boost::noncopyable > ("SI570Slave", no_init )
            .def("configure", &mp7::SI570Slave::configure)
            ;
    
    // Wrap SI570Node
    class_<mp7::SI570Node2g, bases<mp7::SI570Slave, mp7::opencores::I2CBaseNode> > ("SI570Node2g", init<const uhal::Node&>())
            // .def("configure", &mp7::SI570Node2g::configure)
            ;
    
    // Wrap SI5326Slave
    class_<mp7::SI5326Slave, bases<mp7::opencores::I2CSlave>, boost::noncopyable > ("SI5326Slave", no_init )
            .def("configure", &mp7::SI5326Slave::configure)
            .def("reset", &mp7::SI5326Slave::reset)
            .def("intcalib", &mp7::SI5326Slave::intcalib)
            .def("sleep", &mp7::SI5326Slave::sleep)
            .def("debug", &mp7::SI5326Slave::debug)
            .def("registers", &mp7::SI5326Slave::registers)
            ;
    
    // Wrap SI570Node
    class_<mp7::SI5326Node2g, bases<mp7::SI5326Slave, mp7::opencores::I2CBaseNode> > ("SI5326Node2g", init<const uhal::Node&>())
            // .def("configure", &mp7::SI5326Node2g::configure)s
            ;

    // Wrap SI5326Node
    class_<mp7::SI5326Node, bases<mp7::OpenCoresI2C> > ("SI5326Node", init<const uhal::Node&>())
            .def(init<const uhal::Node&, uint8_t>())
            .def("configure", &mp7::SI5326Node::configure)
            .def("reset", &mp7::SI5326Node::reset)
            .def("intcalib", &mp7::SI5326Node::intcalib)
            .def("sleep", &mp7::SI5326Node::sleep)
            .def("debug", &mp7::SI5326Node::debug)
            .def("registers", &mp7::SI5326Node::registers)
            ;
    
    // Wrap SI570Node
    class_<mp7::SI570Node, bases<mp7::OpenCoresI2C> > ("SI570Node", init<const uhal::Node&>())
            .def(init<const uhal::Node&, uint8_t>())
            .def("configure", &mp7::SI570Node::configure)
            ;

    // Wrap the GTXQuadNode
    class_<mp7::GTXQuadNode, bases<uhal::Node> > ("GTXQuadNode", init<const uhal::Node&>())
            .def("configure", &mp7::GTXQuadNode::configure)
            .def("softReset", &mp7::GTXQuadNode::softReset)
            .def("resetFSMs", &mp7::GTXQuadNode::resetFSMs)
            .def("alignOnce", &mp7::GTXQuadNode::alignOnce)
            .def("check", &mp7::GTXQuadNode::check)
            ;
    // Wrap the GTHQuadNode
    class_<mp7::GTHQuadNode, bases<uhal::Node> > ("GTHQuadNode", init<const uhal::Node&>())
            .def("exists", &mp7::GTHQuadNode::exists)
            .def("configure", &mp7::GTHQuadNode::configure)
            .def("loopback", &mp7::GTHQuadNode::loopback)
            .def("softReset", &mp7::GTHQuadNode::softReset)
            .def("resetFSMs", &mp7::GTHQuadNode::resetFSMs)
            .def("clear", &mp7::GTHQuadNode::clear)
            .def("checkQuad", &mp7::GTHQuadNode::checkQuad)
            .def("checkChannel", &mp7::GTHQuadNode::checkChannel)
            .def("check", &mp7::GTHQuadNode::check)
            ;

    {
        // Wrap BufferManager
        scope mp7_BufferNode_scope = class_<mp7::ChanBufferNode, bases<uhal::Node> > ("ChanBufferNode", init<const uhal::Node&>())
                .def("getSize", &mp7::ChanBufferNode::getSize)
                .def("getMaxSize", &mp7::ChanBufferNode::getMaxSize)
                .def("setSize", (void (mp7::ChanBufferNode::*) (size_t)) 0, mp7_ChanBufferNode_setSize_overloads())
                .def("getBufferMode", &mp7::ChanBufferNode::getBufferMode)
                .def("getDataSrc", &mp7::ChanBufferNode::getDataSrc)
                .def("waitCaptureDone", &mp7::ChanBufferNode::waitCaptureDone)
                .def("configure", &mp7::ChanBufferNode::configure)
                .def("setRange", &mp7::ChanBufferNode::setRange)
                .def("clear", &mp7::ChanBufferNode::clear)
                .def("upload", &mp7::ChanBufferNode::upload)
                .def("download", &mp7::ChanBufferNode::download)
                .def("uploadValid", &mp7::ChanBufferNode::uploadValid)
                .def("downloadValid", &mp7::ChanBufferNode::downloadValid)
                .def("readRaw", &mp7::ChanBufferNode::readRaw)
                .def("writeRaw", &mp7::ChanBufferNode::writeRaw)
                ;

        enum_<mp7::ChanBufferNode::BufMode> ("BufMode")
                .value("Latency", mp7::ChanBufferNode::Latency)
                .value("Capture", mp7::ChanBufferNode::Capture)
                .value("PlayOnce", mp7::ChanBufferNode::PlayOnce)
                .value("PlayLoop", mp7::ChanBufferNode::PlayLoop)
                .export_values()
                ;

        enum_<mp7::ChanBufferNode::DataSrc> ("DataSrc")
                .value("Input", mp7::ChanBufferNode::Input)
                .value("Buffer", mp7::ChanBufferNode::Buffer)
                .value("Pattern", mp7::ChanBufferNode::Pattern)
                .value("Zeroes", mp7::ChanBufferNode::Zeroes)
                .export_values()
                ;
    }

    // Wrap the Datapath
    {
        // Datapath scoping
        scope mp7_DatapathNode_scope = class_<mp7::DatapathNode, bases<uhal::Node> > ("DatapathNode", init<const uhal::Node&>())
                .def(init<const mp7::DatapathNode&>())
                .def("drpManager", &mp7::DatapathNode::drpManager, return_value_policy<copy_const_reference>())
                ;
    }
    // Wrap the PPRamNode
    class_<mp7::PPRamNode, bases<uhal::Node> > ("PPRamNode", init<const uhal::Node&>())
            .def("upload", &mp7::PPRamNode::upload)
            //  .def( "writePayload", &mp7::PPRamNode::writePayload )
            .def("dumpRam", &mp7::PPRamNode::dumpRAM)
            .def("upload64", &mp7::PPRamNode::upload64)
            ;
    // Wrap the MGTAlignmentNode
    class_<mp7::AlignmentNode, bases<uhal::Node> > ("AlignmentNode", init<const uhal::Node&>())
            .def("align", (void ( mp7::AlignmentNode::*) (bool)) 0, mp7_AlignmentNode_align_overloads())
            .def("enable", &mp7::AlignmentNode::enable)
            .def("check", &mp7::AlignmentNode::check)
            ;
    // Wrap the DemuxNode
    class_<mp7::DemuxNode, bases<uhal::Node> > ("DemuxNode", init<const uhal::Node&>())
            ;

    // Wrap MiniPODNode
    class_<mp7::MiniPODNode, bases<mp7::OpenCoresI2C> > ("MiniPODNode", init<const uhal::Node&>())
            .def("get3v3", &mp7::MiniPODNode::get3v3)
            .def("get2v5", &mp7::MiniPODNode::get2v5)
            .def("getTemp", &mp7::MiniPODNode::getTemp)
            .def("getOnTime", &mp7::MiniPODNode::getOnTime)
            .def("getOpticalPowers", &mp7::MiniPODNode::getOpticalPowers)
            .def("setChannelPolarity", &mp7::MiniPODNode::setChannelPolarity)
            .def("disableChannel", &mp7::MiniPODNode::disableChannel)
            .def("disableSquelch", &mp7::MiniPODNode::disableSquelch)
            .def("getAlarmTemp", &mp7::MiniPODNode::getAlarmTemp)
            .def("getAlarm3v3", &mp7::MiniPODNode::getAlarm3v3)
            .def("getAlarm2v5", &mp7::MiniPODNode::getAlarm2v5)
            .def("getAlarmLOS", &mp7::MiniPODNode::getAlarmLOS)
            .def("getAlarmOpticalPower", &mp7::MiniPODNode::getAlarmOpticalPower)
            .def("getInfo", &mp7::MiniPODNode::getInfo)
            ;

    //Wrap Measurement
    class_<mp7::Measurement> ("Measurement", init< const double&, const std::string&, const double&, const std::string&>())
            .def(self_ns::str(self))
            ;

    //Wrap MmcpipeInterface
    class_<mp7::MmcPipeInterface, bases<uhal::Node> > ("MmcPipeInterface", init<const uhal::Node&>())
            .def("SetDummySensor", &mp7::MmcPipeInterface::SetDummySensor)
            .def("FileToSD", &mp7::MmcPipeInterface::FileToSD)
            .def("FileFromSD", &mp7::MmcPipeInterface::FileFromSD)
            .def("RebootFPGA", &mp7::MmcPipeInterface::RebootFPGA)
            .def("BoardHardReset", &mp7::MmcPipeInterface::BoardHardReset)
            .def("DeleteFromSD", &mp7::MmcPipeInterface::DeleteFromSD)
            .def("ListFilesOnSD", &mp7::MmcPipeInterface::ListFilesOnSD)
            .def("GetTextSpace", &mp7::MmcPipeInterface::GetTextSpace)
            .def("UpdateCounters", &mp7::MmcPipeInterface::UpdateCounters)
            .def("FPGAtoMMCDataAvailable", &mp7::MmcPipeInterface::FPGAtoMMCDataAvailable, return_value_policy<copy_const_reference>())
            .def("FPGAtoMMCSpaceAvailable", &mp7::MmcPipeInterface::FPGAtoMMCSpaceAvailable, return_value_policy<copy_const_reference>())
            .def("MMCtoFPGADataAvailable", &mp7::MmcPipeInterface::MMCtoFPGADataAvailable, return_value_policy<copy_const_reference>())
            .def("MMCtoFPGASpaceAvailable", &mp7::MmcPipeInterface::MMCtoFPGASpaceAvailable, return_value_policy<copy_const_reference>())
            ;

    //Vector indexing for uint8_t
    class_<std::vector<uint8_t> >("vec_uint8_t")
            .def(vector_indexing_suite< std::vector<uint8_t> >())
            ;

    //Wrap Firmware
    class_<mp7::Firmware> ("Firmware", init<const std::string&>())
            .def("Bitstream", &mp7::Firmware::Bitstream, return_value_policy<copy_const_reference>())
            .def("FileName", &mp7::Firmware::FileName, return_value_policy<copy_const_reference>())
            .def("isBitSwapped", &mp7::Firmware::isBitSwapped, return_value_policy<copy_const_reference>())
            .def("BitSwap", &mp7::Firmware::BitSwap)
            ;


    //Wrap Xilinx stuff
    class_<mp7::XilinxBitStream, bases<mp7::Firmware> > ("XilinxBitStream")
            .def("BigEndianAppend", &mp7::XilinxBitStream::BigEndianAppend)
            ;
    class_<mp7::XilinxBitFile, bases<mp7::Firmware> > ("XilinxBitFile", init<const std::string&>())
            .def("DesignName", &mp7::XilinxBitFile::DesignName, return_value_policy<copy_const_reference>())
            .def("DeviceName", &mp7::XilinxBitFile::DeviceName, return_value_policy<copy_const_reference>())
            .def("TimeStamp", &mp7::XilinxBitFile::TimeStamp, return_value_policy<copy_const_reference>())
            .def("StandardizedFileName", &mp7::XilinxBitFile::StandardizedFileName)
            ;
    class_<mp7::XilinxBinFile, bases<mp7::Firmware> > ("XilinxBinFile", init<const std::string&>())
            ;
}




