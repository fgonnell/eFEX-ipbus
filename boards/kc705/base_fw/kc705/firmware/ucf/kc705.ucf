# Constraints specific to kc705

INST infra/eth/phy/*/gtxe2_i LOC=GTXE2_CHANNEL_X0Y10;

##-------------------------------------------------------------------------------------
## Trigger Path clock domain jump from TTC based clock to link clock
##-------------------------------------------------------------------------------------

# Async output flipflops used for 240MHz to 250MHz domain crossing in Tx path
INST "*tx_clk_bridge/data_out*" TNM = "tx_cdc_downstream_ff";
# Async input flipflops for 240MHz to 250MHz clk domain crossing.
INST "*tx_clk_bridge/buf*" TNM = "tx_cdc_upstream_ff";
# Data will be latched into link clk domain after a minimum of 4.0 ns
# Data valid sampled on downstream clk rising edge, then falling edge.
# Data registered on next rising edge of downstream clk at which time it must be stable.
TIMESPEC TS_tx_cdc = FROM "tx_cdc_upstream_ff" TO "tx_cdc_downstream_ff" 4.0 ns DATAPATHONLY; 

INST ttc/clocks/mmcm LOC=MMCME2_ADV_X0Y5;

NET REFCLK* DIFF_TERM=TRUE | TNM_NET=mgtclk_125;
TIMESPEC TS_mgtclk_125 = PERIOD mgtclk_125 8ns;
NET "*/quad_wrapper_inst/txusrclk*" TNM_NET=mgtclk_250;
NET "*/quad_wrapper_inst/rxusrclk*" TNM_NET=mgtclk_250;
TIMESPEC TS_mgtclk_250 = PERIOD mgtclk_250 4.ns;

AREA_GROUP ctrl RANGE=CLOCKREGION_X1Y5:CLOCKREGION_X1Y4;
AREA_GROUP readout RANGE=CLOCKREGION_X0Y5:CLOCKREGION_X0Y4;
AREA_GROUP payload RANGE=SLICE_X0Y200:SLICE_X89Y349;
AREA_GROUP payload RANGE=RAMB36_X1Y40:RAMB36_X5Y69;
AREA_GROUP payload RANGE=RAMB18_X0Y80:RAMB18_X4Y139;
AREA_GROUP payload RANGE=DSP48_X0Y80:DSP48_X4Y139;
AREA_GROUP align RANGE=CLOCKREGION_X0Y2:CLOCKREGION_X1Y2;
AREA_GROUP align_regs RANGE=SLICE_X0Y100:SLICE_X1Y349;

NET REFCLKP<0> LOC=J8; #mgtrefclk1p_117
NET REFCLKN<0> LOC=J7; #mgtrefclk1n_117

# Area group definitions

AREA_GROUP payload_0 RANGE=SLICE_X0Y300 : SLICE_X73Y349;
AREA_GROUP payload_1 RANGE=SLICE_X0Y200 : SLICE_X73Y249;

AREA_GROUP quad_x0y3 RANGE=SLICE_X74Y300: SLICE_X145Y349;
AREA_GROUP quad_x0y1 RANGE=SLICE_X74Y200: SLICE_X145Y249;

AREA_GROUP quad_x0y3 RANGE=RAMB18_X3Y120: RAMB18_X5Y139;
AREA_GROUP quad_x0y1 RANGE=RAMB18_X3Y80 : RAMB18_X5Y99;

INST "datapath/align/align/*" AREA_GROUP=align; 
INST "datapath/align/reg/*" AREA_GROUP=align; 
# Force ISE to place additional regs to improve timing in a sensible 
# location by reducing the degrees of freedom to just the Y dimension 
INST "datapath/align/align_reg/*" AREA_GROUP=align_regs;
#INST "datapath/align_mon/*" AREA_GROUP=readout;

# Datapath constraints

INST "datapath/rgen[0]*" AREA_GROUP=quad_x0y3;
INST "datapath/rgen[1]*" AREA_GROUP=quad_x0y1;

INST datapath/rgen[0].region/*/g_gt_instances[0]*/gtxe2_i LOC=GTXE2_CHANNEL_X0Y12;
INST datapath/rgen[0].region/*/g_gt_instances[1]*/gtxe2_i LOC=GTXE2_CHANNEL_X0Y13;
INST datapath/rgen[0].region/*/g_gt_instances[2]*/gtxe2_i LOC=GTXE2_CHANNEL_X0Y14;
INST datapath/rgen[0].region/*/g_gt_instances[3]*/gtxe2_i LOC=GTXE2_CHANNEL_X0Y15;
INST datapath/rgen[0].region/*/gtxe2_common_i LOC=GTXE2_COMMON_X0Y3;

INST datapath/rgen[1].region/*/g_gt_instances[0]*/gtxe2_i LOC=GTXE2_CHANNEL_X0Y4;
INST datapath/rgen[1].region/*/g_gt_instances[1]*/gtxe2_i LOC=GTXE2_CHANNEL_X0Y5;
INST datapath/rgen[1].region/*/g_gt_instances[2]*/gtxe2_i LOC=GTXE2_CHANNEL_X0Y6;
INST datapath/rgen[1].region/*/g_gt_instances[3]*/gtxe2_i LOC=GTXE2_CHANNEL_X0Y7;
INST datapath/rgen[1].region/*/gtxe2_common_i LOC=GTXE2_COMMON_X0Y1;
