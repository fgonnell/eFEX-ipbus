-- top_decl
--
-- Defines constants for the whole device
--
-- Dave Newbold, June 2014

library IEEE;
use IEEE.STD_LOGIC_1164.all;

package top_decl is
  
	constant LHC_BUNCH_COUNT: integer := 3564;
	constant LB_ADDR_WIDTH: integer := 10;
	constant DR_ADDR_WIDTH: integer := 9;
	constant RO_ADDR_WIDTH: integer := 15;
	constant N_REGION: integer := 2; --18;
	constant ALIGN_REGION: integer := 0; --4;
	constant CROSS_REGION: integer := 8;
	constant CLOCK_RATIO: integer := 6;
	constant N_REFCLK: integer := 1;

	type region_kind_t is (empty, buf_only, gth_10g);
	
	type region_conf_t is
		record
			kind: region_kind_t;
			xloc: integer;
			yloc: integer;
			refclk: integer range 0 to N_REFCLK - 1;
		end record;
	
	type region_conf_array_t is array(0 to N_REGION - 1) of region_conf_t;

	constant REGION_CONF: region_conf_array_t := (
		(gth_10g, 0, 3, 0), -- 0 / 118		--		(gth_10g, 1, 8, 3), -- 0 / 118
		(gth_10g, 0, 1, 0)  -- 1 / 116		--		(gth_10g, 1, 7, 3) -- 1 / 117*

--		(gth_10g, 1, 6, 4), -- 2 / 116
--		(gth_10g, 1, 5, 4), -- 3 / 115*
--		(gth_10g, 1, 4, 5), -- 4 / 114
--		(gth_10g, 1, 3, 5), -- 5 / 113*
--		(gth_10g, 1, 2, 7), -- 6 / 112
--		(gth_10g, 1, 1, 7), -- 7 / 111*
--		(gth_10g, 1, 0, 7), -- 8 / 110
--		(gth_10g, 0, 0, 6), -- 9 / 210
--		(gth_10g, 0, 1, 6), -- 10 / 211*
--		(gth_10g, 0, 2, 2), -- 11 / 212
--		(gth_10g, 0, 3, 2), -- 12 / 213*
--		(gth_10g, 0, 4, 1), -- 13 / 214
--		(gth_10g, 0, 5, 1), -- 14 / 215*
--		(gth_10g, 0, 6, 0), -- 15 / 216
--		(gth_10g, 0, 7, 0), -- 16 / 217*
--		(gth_10g, 0, 8, 0) -- 17 / 218
----		(gth_10g, 0, 9, 8) -- 18 / 219
	);

end top_decl;

